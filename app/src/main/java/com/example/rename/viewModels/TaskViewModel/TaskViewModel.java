package com.example.rename.viewModels.TaskViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.TaskAdapter;
import com.example.rename.entitiesModel.entities.InsertChar;
import com.example.rename.entitiesModel.entities.InsertFileSize;
import com.example.rename.entitiesModel.entities.InsertNumber;
import com.example.rename.entitiesModel.entities.NewName;
import com.example.rename.entitiesModel.entities.ReplaceChars;
import com.example.rename.entitiesModel.entities.Task;
import com.example.rename.repository.TaskRepository;

import java.util.List;


public class TaskViewModel extends AndroidViewModel {

    private TaskRepository mTaskRepository;

    public TaskViewModel(@NonNull Application application) {
        super(application);

        mTaskRepository = TaskRepository.getInstance();
    }

    public LiveData<List<TaskAdapter.DataHolder>> getTaskAdapterListLiveData() {
        return mTaskRepository.getTaskAdapterListLiveData();
    }

    public Task loadTaskAdapterByIdList(Long taskID) {
        return mTaskRepository.loadTaskAdapterByIdList(getApplication(), taskID);
    }

    public LiveData<List<TaskAdapter.DataHolder>> getTaskAdapterListDialogLiveData(){
        return mTaskRepository.getTaskAdapterListDialogLiveData();
    }

    public void loadTaskAdapterListDialogLiveData(){
        mTaskRepository.loadTaskAdapterListDialogLiveData(getApplication().getBaseContext());
    }

    public String getTempTaskTitle() {
        return mTaskRepository.getTempTaskTitle();
    }

    public void setTempTaskTitle(String tempTaskTitle) {
        mTaskRepository.setTempTaskTitle(tempTaskTitle);
    }


    public void newTask(String title, Task newTask){
        mTaskRepository.newTask(getApplication(), title, newTask);
    }

    public void addTempCriteriaToLiveData(ReplaceChars replaceChars, NewName newName,
                                          InsertNumber insertNumber, InsertFileSize insertFileSize,
                                          InsertChar insertChar){
        mTaskRepository.addTempCriteriaToLiveData(getApplication(), replaceChars, newName, insertNumber, insertFileSize, insertChar);
    }

    public void deleteCriteria(Long id) {
        mTaskRepository.deleteCriteria(getApplication(), id);
    }

    public void deleteTask(Long id){ mTaskRepository.deleteTask(getApplication(), id);}

    public MutableLiveData<Task> getTempTaskLiveData() {
        return mTaskRepository.getTempTaskLiveData();
    }

    public void setTempTaskLiveData(Task tempTask) {
        mTaskRepository.setTempTaskLiveData(tempTask);
    }

    public void resetAllVariables(){
        mTaskRepository.resetAllVariables();
    }
}
