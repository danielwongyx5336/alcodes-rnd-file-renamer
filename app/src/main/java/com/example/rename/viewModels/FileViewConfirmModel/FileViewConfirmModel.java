package com.example.rename.viewModels.FileViewConfirmModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.FileAdapter_Confirm;
import com.example.rename.entitiesModel.entities.file;
import com.example.rename.repository.FileConfirmRepository;

import java.util.List;


public class FileViewConfirmModel extends AndroidViewModel {
    private FileConfirmRepository mFileConfirmRepository;

    public FileViewConfirmModel(@NonNull Application application){
        super(application);

        mFileConfirmRepository = FileConfirmRepository.getInstance();
    }

    public LiveData<List<FileAdapter_Confirm.DataHolder>> getFileAdapterListLiveData() {
        return mFileConfirmRepository.getFileAdapterListLiveData();
    }

    public void loadFileAdapterList(List<file> fileSelectedRecords) {
        mFileConfirmRepository.loadFileAdapterList(fileSelectedRecords);
    }

    public void deleteFile(String Id){
        mFileConfirmRepository.deleteFile(Id);
    }

    public LiveData<List<file>> getmFileSelectionConfirmationList(){
        return mFileConfirmRepository.getmFileSelectionConfirmationList();
    }

    public List<file> getFileList_Confirm() {
        return mFileConfirmRepository.getFileList_Confirm();
    }

    public void setFileList_Confirm(List<file> fileList_Confirm) {
        mFileConfirmRepository.setFileList_Confirm(fileList_Confirm);
    }

    public void resetAllVariables() {
        mFileConfirmRepository.resetAllVariables();
    }
}
