package com.example.rename.viewModels.PreviewViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


public class PreviewViewModelFactory implements ViewModelProvider.Factory {

    private Application mApplication;

    public PreviewViewModelFactory(Application application) {
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new PreviewViewModel(mApplication);
    }
}
