package com.example.rename.viewModels.FileViewSelectModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.FileAdapter_Select;
import com.example.rename.entitiesModel.entities.file;
import com.example.rename.repository.FileSelectRepository;

import java.util.List;


public class FileViewSelectModel extends AndroidViewModel {
    private FileSelectRepository mFileSelectRepository;

    public FileViewSelectModel(@NonNull Application application){
        super(application);

        mFileSelectRepository = FileSelectRepository.getInstance();
    }

    public LiveData<List<FileAdapter_Select.DataHolder>> getFileAdapterListLiveData() {
        return mFileSelectRepository.getFileAdapterListLiveData();
    }

    public void loadFileAdapterList(List<file> fileSelectedRecords) {
        mFileSelectRepository.loadFileAdapterList(fileSelectedRecords);
    }

    public void deleteFile(String Id){
        mFileSelectRepository.deleteFile(Id);
    }

    public List<file> getFileList_Select() {
        return mFileSelectRepository.getFileList_Select();
    }

    public void setFileList_Select(List<file> fileList_Select) {
        mFileSelectRepository.setFileList_Select(fileList_Select);
    }

    public void resetAllVariables(){
        mFileSelectRepository.resetAllVariables();
    }
}
