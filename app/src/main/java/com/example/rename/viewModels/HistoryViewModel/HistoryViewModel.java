package com.example.rename.viewModels.HistoryViewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.HistoryAdapter;
import com.example.rename.adapters.PreviewHistoryAdapter;
import com.example.rename.entitiesModel.entities.RenameHistory;
import com.example.rename.repository.HistoryRepository;

import java.util.List;


public class HistoryViewModel extends AndroidViewModel {
    private HistoryRepository mHistoryRepository;

    public HistoryViewModel(@NonNull Application application){
        super(application);

        mHistoryRepository = HistoryRepository.getInstance();
    }

    public MutableLiveData<List<HistoryAdapter.DataHolder>> getHistoryAdapterListLiveData() {
        return mHistoryRepository.getHistoryAdapterListLiveData();
    }

    public void setHistoryAdapterListLiveData(List<HistoryAdapter.DataHolder> mHistoryAdapterListLiveData){
        mHistoryRepository.setHistoryAdapterListLiveData(mHistoryAdapterListLiveData);
    }

    public void loadHistoryAdapterList() {
        mHistoryRepository.loadHistoryAdapterList(getApplication());
    }

    public MutableLiveData<List<PreviewHistoryAdapter.DataHolder>> getPreviewHistoryListLiveData() {
        return mHistoryRepository.getPreviewHistoryListLiveData();
    }

    public void setPreviewHistoryListLiveData(List<PreviewHistoryAdapter.DataHolder> mPreviewHistoryListLiveData) {
        mHistoryRepository.setPreviewHistoryListLiveData(mPreviewHistoryListLiveData);
    }

    public void loadSingleHistory(Long ID) {
        mHistoryRepository.loadSingleHistory(getApplication(), ID);
    }
}
