package com.example.rename.viewModels.FileViewConfirmModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class FileViewConfirmModelFactory implements ViewModelProvider.Factory {

    private Application mApplication;

    public FileViewConfirmModelFactory(Application application) {
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new FileViewConfirmModel(mApplication);
    }
}
