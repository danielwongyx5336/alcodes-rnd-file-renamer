package com.example.rename.viewModels.PreviewViewModel;

import android.app.Application;
import android.content.ClipData;
import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.PreviewAdapter;
import com.example.rename.adapters.TaskAdapter;
import com.example.rename.entitiesModel.entities.file;
import com.example.rename.entitiesModel.entities.Task;
import com.example.rename.repository.PreviewRepository;

import java.util.List;


public class PreviewViewModel extends AndroidViewModel {
    private PreviewRepository mPreviewRepository;

    public PreviewViewModel(@NonNull Application application){
        super(application);

        mPreviewRepository = PreviewRepository.getInstance();
    }

    public LiveData<List<PreviewAdapter.DataHolder>> getFileAdapterListLiveData() {
        return mPreviewRepository.getmPreviewAdapterLiveData();
    }

    public void loadPreviewAdapterList(List<file> file, Task tempTask) {
        mPreviewRepository.loadPreviewAdapterList(getApplication(), file, tempTask);
    }

    public LiveData<List<PreviewAdapter.DataHolder>> getmFileSelectionConfirmationList(){
        return mPreviewRepository.getmPreviewAdapterLiveData();
    }

    public Boolean renameProcess(){
        return mPreviewRepository.renameProcess(getApplication());
    }

    public Boolean fileRenamedOutputLocation(Uri uri){
        return mPreviewRepository.fileRenamedOutputLocation(getApplication(), uri);
    }

    public MutableLiveData<String> getOutputPath() {
        return mPreviewRepository.getOutputPath();
    }

    public void setOutputPath(String outputPath) {
        mPreviewRepository.setOutputPath(outputPath);
    }

    public void savedPreviewToHistory(){
        mPreviewRepository.savedPreviewToHistory(getApplication());
    }
}
