package com.example.rename.entitiesModel.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

@Entity
public class InsertNumber{
    @Id(autoincrement = true)
    private Long Id;

    @NotNull
    private String title;

    @NotNull
    private Integer insertIndex;

    @NotNull
    private Double insertNumbers;

    @NotNull
    private Long taskID;

    @Generated(hash = 1704148173)
    public InsertNumber(Long Id, @NotNull String title,
            @NotNull Integer insertIndex, @NotNull Double insertNumbers,
            @NotNull Long taskID) {
        this.Id = Id;
        this.title = title;
        this.insertIndex = insertIndex;
        this.insertNumbers = insertNumbers;
        this.taskID = taskID;
    }

    @Generated(hash = 1787458044)
    public InsertNumber() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Integer getInsertIndex() {
        return this.insertIndex;
    }

    public void setInsertIndex(Integer insertIndex) {
        this.insertIndex = insertIndex;
    }

    public Double getInsertNumbers() {
        return this.insertNumbers;
    }

    public void setInsertNumbers(Double insertNumbers) {
        this.insertNumbers = insertNumbers;
    }

    public Long getTaskID() {
        return this.taskID;
    }

    public void setTaskID(Long taskID) {
        this.taskID = taskID;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
