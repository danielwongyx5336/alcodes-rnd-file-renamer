package com.example.rename.entitiesModel.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

@Entity
public class NewName {
    @Id(autoincrement = true)
    private Long Id;

    @NotNull
    private String title;

    @NotNull
    private String newName;

    @NotNull
    private Long taskID;

    @Generated(hash = 1809527530)
    public NewName(Long Id, @NotNull String title, @NotNull String newName,
            @NotNull Long taskID) {
        this.Id = Id;
        this.title = title;
        this.newName = newName;
        this.taskID = taskID;
    }

    @Generated(hash = 991206095)
    public NewName() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getNewName() {
        return this.newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public Long getTaskID() {
        return this.taskID;
    }

    public void setTaskID(Long taskID) {
        this.taskID = taskID;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
