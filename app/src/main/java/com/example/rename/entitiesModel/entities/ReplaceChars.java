package com.example.rename.entitiesModel.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

@Entity
public class ReplaceChars {
    @Id(autoincrement = true)
    private Long Id;

    @NotNull
    private String title;

    @NotNull
    private String oldCharacter;

    @NotNull
    private String replaceCharacter;

    @NotNull
    private Long taskID;

    @Generated(hash = 812747341)
    public ReplaceChars(Long Id, @NotNull String title,
            @NotNull String oldCharacter, @NotNull String replaceCharacter,
            @NotNull Long taskID) {
        this.Id = Id;
        this.title = title;
        this.oldCharacter = oldCharacter;
        this.replaceCharacter = replaceCharacter;
        this.taskID = taskID;
    }

    @Generated(hash = 2146708623)
    public ReplaceChars() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getReplaceCharacter() {
        return this.replaceCharacter;
    }

    public void setReplaceCharacter(String replaceCharacter) {
        this.replaceCharacter = replaceCharacter;
    }

    public Long getTaskID() {
        return this.taskID;
    }

    public void setTaskID(Long taskID) {
        this.taskID = taskID;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOldCharacter() {
        return this.oldCharacter;
    }

    public void setOldCharacter(String oldCharacter) {
        this.oldCharacter = oldCharacter;
    }

}
