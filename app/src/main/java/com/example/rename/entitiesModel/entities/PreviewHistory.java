package com.example.rename.entitiesModel.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

import org.greenrobot.greendao.annotation.Generated;

@Entity
public class PreviewHistory {
    @Id(autoincrement = true)
    private Long Id;

    private String taskTitle;

    private String originalFileName;

    private String newFileName;

    private String sourceFileName;

    @NotNull
    private Long renameHistoryID;

    @Generated(hash = 571915125)
    public PreviewHistory(Long Id, String taskTitle, String originalFileName,
            String newFileName, String sourceFileName,
            @NotNull Long renameHistoryID) {
        this.Id = Id;
        this.taskTitle = taskTitle;
        this.originalFileName = originalFileName;
        this.newFileName = newFileName;
        this.sourceFileName = sourceFileName;
        this.renameHistoryID = renameHistoryID;
    }

    @Generated(hash = 1240784169)
    public PreviewHistory() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getTaskTitle() {
        return this.taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getOriginalFileName() {
        return this.originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getNewFileName() {
        return this.newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public String getSourceFileName() {
        return this.sourceFileName;
    }

    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }

    public Long getRenameHistoryID() {
        return this.renameHistoryID;
    }

    public void setRenameHistoryID(Long renameHistoryID) {
        this.renameHistoryID = renameHistoryID;
    }
}
