package com.example.rename.entitiesModel.entities;

import androidx.annotation.Nullable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.OrderBy;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.ArrayList;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Unique;

@Entity
public class Task {
    @Id(autoincrement = true)
    private Long Id;

    @NotNull
    private String title;

    private Boolean enableFlag = true;

    @ToMany(referencedJoinProperty = "taskID")
    @Nullable
    private List<ReplaceChars> replaceChars;

    @ToMany(referencedJoinProperty = "taskID")
    @Nullable
    private List<NewName> newName;

    @ToMany(referencedJoinProperty = "taskID")
    @Nullable
    private List<InsertChar> insertChar;

    @ToMany(referencedJoinProperty = "taskID")
    @Nullable
    private List<InsertNumber> insertNumbers;

    @ToMany(referencedJoinProperty = "taskID")
    @Nullable
    private List<InsertFileSize> insertFileSize;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1469429066)
    private transient TaskDao myDao;

    @Generated(hash = 1978103016)
    public Task(Long Id, @NotNull String title, Boolean enableFlag) {
        this.Id = Id;
        this.title = title;
        this.enableFlag = enableFlag;
    }

    @Generated(hash = 733837707)
    public Task() {
    }

    public Task(String title){
        this.title = title;
        this.replaceChars = new ArrayList<>();
        this.newName = new ArrayList<>();
        this.insertChar = new ArrayList<>();
        this.insertNumbers = new ArrayList<>();
        this.insertFileSize = new ArrayList<>();
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 906467279)
    public List<ReplaceChars> getReplaceChars() {
        if (replaceChars == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ReplaceCharsDao targetDao = daoSession.getReplaceCharsDao();
            List<ReplaceChars> replaceCharsNew = targetDao._queryTask_ReplaceChars(Id);
            synchronized (this) {
                if (replaceChars == null) {
                    replaceChars = replaceCharsNew;
                }
            }
        }
        return replaceChars;
    }

    public List<ReplaceChars> getReplaceCharsNoDB(){
        return this.replaceChars;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1089043659)
    public synchronized void resetReplaceChars() {
        replaceChars = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 959401011)
    public List<NewName> getNewName() {
        if (newName == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            NewNameDao targetDao = daoSession.getNewNameDao();
            List<NewName> newNameNew = targetDao._queryTask_NewName(Id);
            synchronized (this) {
                if (newName == null) {
                    newName = newNameNew;
                }
            }
        }
        return newName;
    }

    public List<NewName> getNewNameNoDb(){
        return this.newName;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 107598985)
    public synchronized void resetNewName() {
        newName = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1291241837)
    public List<InsertChar> getInsertChar() {
        if (insertChar == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            InsertCharDao targetDao = daoSession.getInsertCharDao();
            List<InsertChar> insertCharNew = targetDao._queryTask_InsertChar(Id);
            synchronized (this) {
                if (insertChar == null) {
                    insertChar = insertCharNew;
                }
            }
        }
        return insertChar;
    }

    public List<InsertChar> getInsertCharNoDb(){
        return this.insertChar;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1178271633)
    public synchronized void resetInsertChar() {
        insertChar = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1081156446)
    public List<InsertNumber> getInsertNumbers() {
        if (insertNumbers == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            InsertNumberDao targetDao = daoSession.getInsertNumberDao();
            List<InsertNumber> insertNumbersNew = targetDao._queryTask_InsertNumbers(Id);
            synchronized (this) {
                if (insertNumbers == null) {
                    insertNumbers = insertNumbersNew;
                }
            }
        }
        return insertNumbers;
    }

    public List<InsertNumber> getInsertNumberNoDb(){
        return this.insertNumbers;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 205562288)
    public synchronized void resetInsertNumbers() {
        insertNumbers = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1068867461)
    public List<InsertFileSize> getInsertFileSize() {
        if (insertFileSize == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            InsertFileSizeDao targetDao = daoSession.getInsertFileSizeDao();
            List<InsertFileSize> insertFileSizeNew = targetDao._queryTask_InsertFileSize(Id);
            synchronized (this) {
                if (insertFileSize == null) {
                    insertFileSize = insertFileSizeNew;
                }
            }
        }
        return insertFileSize;
    }

    public List<InsertFileSize> getInsertFileSizeNoDb(){
        return this.insertFileSize;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1964949281)
    public synchronized void resetInsertFileSize() {
        insertFileSize = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public Boolean getEnableFlag() {
        return this.enableFlag;
    }

    public void setEnableFlag(Boolean enableFlag) {
        this.enableFlag = enableFlag;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1442741304)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getTaskDao() : null;
    }
}
