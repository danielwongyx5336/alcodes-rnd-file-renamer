package com.example.rename.entitiesModel.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

@Entity
public class InsertFileSize {
    @Id(autoincrement = true)
    private Long Id;

    @NotNull
    private String title;

    @NotNull
    private String unit;

    @NotNull
    private Integer insertIndex;

    @NotNull
    private Long taskID;

    @Generated(hash = 331089081)
    public InsertFileSize(Long Id, @NotNull String title, @NotNull String unit,
            @NotNull Integer insertIndex, @NotNull Long taskID) {
        this.Id = Id;
        this.title = title;
        this.unit = unit;
        this.insertIndex = insertIndex;
        this.taskID = taskID;
    }

    @Generated(hash = 1846507418)
    public InsertFileSize() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getInsertIndex() {
        return this.insertIndex;
    }

    public void setInsertIndex(Integer insertIndex) {
        this.insertIndex = insertIndex;
    }

    public Long getTaskID() {
        return this.taskID;
    }

    public void setTaskID(Long taskID) {
        this.taskID = taskID;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
