package com.example.rename.entitiesModel.entities;

import android.net.Uri;

public class file {
    private String Id;
    private Uri uri;
    private String filename;
    private String path;
    //File Type
    private String extension;
    private Long fileSize;
    private Boolean confirmFlag;


    public file() {
    }

    public file(String Id, Uri uri, String filename, String path, String extension, Long fileSize, Boolean confirmFlag) {
        this.Id = Id;
        this.uri = uri;
        this.filename = filename;
        this.path = path;
        this.extension = extension;
        this.fileSize = fileSize;
        this.confirmFlag = confirmFlag;
    }

    public file(String Id, Uri uri, String filename, String path, String extension, Long fileSize) {
        this.Id = Id;
        this.uri = uri;
        this.filename = filename;
        this.path = path;
        this.extension = extension;
        this.fileSize = fileSize;
        this.confirmFlag = false;
    }

    public String getId() {
        return Id;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Boolean getConfirmFlag() {
        return confirmFlag;
    }

    public void setConfirmFlag(Boolean confirmFlag) {
        this.confirmFlag = confirmFlag;
    }
}
