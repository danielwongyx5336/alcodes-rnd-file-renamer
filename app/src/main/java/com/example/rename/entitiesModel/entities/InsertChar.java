package com.example.rename.entitiesModel.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.NotNull;

@Entity
public class InsertChar{
    @Id(autoincrement = true)
    private Long Id;

    @NotNull
    private String title;

    @NotNull
    private String insertCharacter;

    @NotNull
    private Integer insertIndex;

    @NotNull
    private Long taskID;

    @Generated(hash = 2097989194)
    public InsertChar(Long Id, @NotNull String title,
            @NotNull String insertCharacter, @NotNull Integer insertIndex,
            @NotNull Long taskID) {
        this.Id = Id;
        this.title = title;
        this.insertCharacter = insertCharacter;
        this.insertIndex = insertIndex;
        this.taskID = taskID;
    }

    @Generated(hash = 107415648)
    public InsertChar() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getInsertCharacter() {
        return this.insertCharacter;
    }

    public void setInsertCharacter(String insertCharacter) {
        this.insertCharacter = insertCharacter;
    }

    public Integer getInsertIndex() {
        return this.insertIndex;
    }

    public void setInsertIndex(Integer insertIndex) {
        this.insertIndex = insertIndex;
    }

    public Long getTaskID() {
        return this.taskID;
    }

    public void setTaskID(Long taskID) {
        this.taskID = taskID;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
