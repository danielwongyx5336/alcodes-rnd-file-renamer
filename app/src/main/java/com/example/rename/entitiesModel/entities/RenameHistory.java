package com.example.rename.entitiesModel.entities;

import androidx.annotation.Nullable;

import com.example.rename.adapters.PreviewAdapter;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
public class RenameHistory {
    @Id(autoincrement = true)
    private Long Id;

    @NotNull
    private String tasktitle;

    @Nullable
    private String outputPath;

    @NotNull
    private Long taskID;

    private Date renameDate;

    @ToOne(joinProperty = "taskID")
    @NotNull
    private Task Task;

    @ToMany(referencedJoinProperty = "renameHistoryID")
    @NotNull
    private List<PreviewHistory> fileNames;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 273322223)
    private transient RenameHistoryDao myDao;


    @Generated(hash = 166676565)
    public RenameHistory(Long Id, @NotNull String tasktitle, String outputPath, @NotNull Long taskID,
            Date renameDate) {
        this.Id = Id;
        this.tasktitle = tasktitle;
        this.outputPath = outputPath;
        this.taskID = taskID;
        this.renameDate = renameDate;
    }


    @Generated(hash = 1619258856)
    public RenameHistory() {
    }


    public Long getId() {
        return this.Id;
    }


    public void setId(Long Id) {
        this.Id = Id;
    }


    public String getTasktitle() {
        return this.tasktitle;
    }


    public void setTasktitle(String tasktitle) {
        this.tasktitle = tasktitle;
    }


    public String getOutputPath() {
        return this.outputPath;
    }


    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }


    public Long getTaskID() {
        return this.taskID;
    }


    public void setTaskID(Long taskID) {
        this.taskID = taskID;
    }


    @Generated(hash = 1567535989)
    private transient Long Task__resolvedKey;


    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1620476513)
    public Task getTask() {
        Long __key = this.taskID;
        if (Task__resolvedKey == null || !Task__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            TaskDao targetDao = daoSession.getTaskDao();
            Task TaskNew = targetDao.load(__key);
            synchronized (this) {
                Task = TaskNew;
                Task__resolvedKey = __key;
            }
        }
        return Task;
    }


    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2019862220)
    public void setTask(@NotNull Task Task) {
        if (Task == null) {
            throw new DaoException(
                    "To-one property 'taskID' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.Task = Task;
            taskID = Task.getId();
            Task__resolvedKey = taskID;
        }
    }


    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 2099066230)
    public List<PreviewHistory> getFileNames() {
        if (fileNames == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            PreviewHistoryDao targetDao = daoSession.getPreviewHistoryDao();
            List<PreviewHistory> fileNamesNew = targetDao._queryRenameHistory_FileNames(Id);
            synchronized (this) {
                if (fileNames == null) {
                    fileNames = fileNamesNew;
                }
            }
        }
        return fileNames;
    }


    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 6387123)
    public synchronized void resetFileNames() {
        fileNames = null;
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }


    public Date getRenameDate() {
        return this.renameDate;
    }


    public void setRenameDate(Date renameDate) {
        this.renameDate = renameDate;
    }


    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 7523541)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getRenameHistoryDao() : null;
    }

}
