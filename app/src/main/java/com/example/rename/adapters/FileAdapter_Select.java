package com.example.rename.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rename.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import java.util.ArrayList;
import java.util.List;

public class FileAdapter_Select extends RecyclerView.Adapter<FileAdapter_Select.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_files, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public String id;
        public String filename;
        public Boolean confirmFlag;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearLayout_item_file)
        public LinearLayout root;

        @BindView(R.id.button_builder_list)
        public AppCompatButton button_builder_list;

        @BindView(R.id.textview_file_name)
        public TextView filename;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                filename.setText(data.filename);

                if (callbacks != null) {
                    button_builder_list.setVisibility(View.VISIBLE);
                    button_builder_list.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onDeleteButtonClicked(data);
                        }
                    });
                }
            }
        }

        public void resetViews() {
            filename.setText("No data");
            button_builder_list.setOnClickListener(null);
        }
    }

    public interface Callbacks {

        void onDeleteButtonClicked(DataHolder data);

    }
}
