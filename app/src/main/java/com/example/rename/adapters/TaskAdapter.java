package com.example.rename.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rename.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_criteria, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {
        public Long id;
        public String title;
        public Long taskID;
        public Long criteriaTableID;
        public String criteriaTableName;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.button_task_list)
        protected AppCompatButton button_task_list;

        @BindView(R.id.textview_task_name)
        protected TextView textView_task_name;

        @BindView(R.id.linearLayout_taskList)
        protected LinearLayout root;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                textView_task_name.setText(data.title);
                button_task_list.setBackgroundResource(R.drawable.gradient_redish);
                button_task_list.setTextColor(Color.WHITE);

                if (callbacks != null) {
                    button_task_list.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onDeleteButtonClicked(data);
                        }
                    });
                }
            }
        }

        public void resetViews() {
            textView_task_name.setText("");
            button_task_list.setOnClickListener(null);
        }
    }

    public interface Callbacks {
        void onDeleteButtonClicked(DataHolder data);
    }
}
