package com.example.rename.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rename.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_preview, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public String taskTitle;
        public String originalFileName;
        public String newFileName;
        public String sourceFileName;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textview_new_filename)
        protected TextView textView_new_filename;

        @BindView(R.id.textview_original_filename)
        protected TextView textView_original_filename;

        @BindView(R.id.textview_previewRenameTask)
        protected TextView textView_preview_rename_task;

        @BindView(R.id.linearLayout_previewList)
        protected LinearLayout root;

        @BindView(R.id.textview_SourceFileName)
        protected TextView textView_SourceFileName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data) {
            resetViews();

            if (data != null) {
                textView_preview_rename_task.setText(data.taskTitle);
                textView_original_filename.setText(data.originalFileName);
                textView_new_filename.setText(data.newFileName);
                textView_SourceFileName.setText(data.sourceFileName);
            }
        }

        public void resetViews() {
            textView_preview_rename_task.setText("");
            textView_original_filename.setText("");
            textView_new_filename.setText("");
            textView_SourceFileName.setText("");
        }
    }

    public interface Callbacks {
        void onDeleteButtonClicked(DataHolder data);
    }
}
