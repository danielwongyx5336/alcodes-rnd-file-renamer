package com.example.rename.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rename.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mCallbacks, mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public String taskTitle;
        public String outputPath;
        public Date renameDate;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textview_history_task_name)
        protected TextView textView_history_task_name;

        @BindView(R.id.textview_output_title)
        protected TextView textview_output_title;

        @BindView(R.id.textview_output_location)
        protected TextView textView_output_location;

        @BindView(R.id.textview_task_rename_date)
        protected TextView textview_task_rename_date;

        @BindView(R.id.linearLayout_historyList)
        protected LinearLayout root;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(Callbacks callbacks, DataHolder data) {
            resetViews();

            if (data != null) {
                textView_history_task_name.setText(data.taskTitle);
                textview_output_title.setText("Output Location:");
                textview_task_rename_date.setText("Recorded Date: " + data.renameDate.toString());

                if(data.outputPath == null || data.outputPath.equals("")){
                    textView_output_location.setText("No Re-position after renamed.");
                }else{
                    textView_output_location.setText(data.outputPath);
                }

                if(callbacks != null){
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                }
            }
        }

        public void resetViews() {
            textView_history_task_name.setText(null);
            textView_output_location.setText(null);
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks {
        void onListItemClicked(DataHolder data);
    }
}
