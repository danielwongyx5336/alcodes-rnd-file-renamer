package com.example.rename.fragments;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.rename.R;
import com.example.rename.adapters.FileAdapter_Confirm;
import com.example.rename.adapters.FileAdapter_Select;
import com.example.rename.entitiesModel.entities.file;
import com.example.rename.utils.FileNameHelper;
import com.example.rename.viewModels.FileViewConfirmModel.FileViewConfirmModelFactory;
import com.example.rename.viewModels.FileViewConfirmModel.FileViewConfirmModel;
import com.example.rename.viewModels.FileViewSelectModel.FileViewSelectModel;
import com.example.rename.viewModels.FileViewSelectModel.FileViewSelectModelFactory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FileSelectionFragment extends Fragment implements FileAdapter_Select.Callbacks, FileAdapter_Confirm.Callbacks {

    public static final String TAG = FileSelectionFragment.class.getSimpleName();

    @BindView(R.id.recycleView_selectionList)
    protected RecyclerView mRecycleView_selectionList;

    @BindView(R.id.recycleView_confirmationList)
    protected RecyclerView mRecycleView_confirmationList;

    @BindView(R.id.button_select_files)
    protected CardView button_select_files;

    @BindView(R.id.button_select_folder)
    protected CardView button_select_folder;

    @BindView(R.id.textView_SelectEmpty)
    protected TextView textView_SelectEmpty;

    @BindView(R.id.textView_ConfirmEmpty)
    protected TextView textview_ConfirmEmpty;

    private Unbinder mUnbinder;
    private FileAdapter_Select mAdapter_select;
    private FileAdapter_Confirm mAdapter_confirm;
    private FileViewSelectModel mViewModel_select;
    private FileViewConfirmModel mViewModel_confirm;
    private FileNameHelper fileNameHelper;

    private List<file> fileList_select;
    private List<file> fileList_confirm;

    private Callbacks mCallbacks;

    private static final int OPEN_FILES_REQUEST_CODE = 40;
    private static final int OPEN_FOLDER_REQUEST_CODE = 41;

    public FileSelectionFragment(){
    }

    public static FileSelectionFragment newInstance() {
        return new FileSelectionFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_file_selection, container, false);

        mUnbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onDeleteButtonClicked(FileAdapter_Select.DataHolder data) {
        for(int i=0; i < fileList_select.size(); i++){
            if(fileList_select.get(i).getId().equals(data.id)){
                fileList_confirm.add(new file(fileList_select.get(i).getId(),
                        fileList_select.get(i).getUri(),
                        fileList_select.get(i).getFilename(),
                        fileList_select.get(i).getPath(),
                        fileList_select.get(i).getExtension(),
                        fileList_select.get(i).getFileSize(), true));
                break;
            }
        }

        mViewModel_confirm.loadFileAdapterList(fileList_confirm);
        mViewModel_select.deleteFile(data.id);

        //Save temp list into mViewModel**
        mViewModel_confirm.setFileList_Confirm(fileList_confirm);
        //Save temp list into mViewModel**
    }

    @Override
    public void onDeleteButtonClicked(FileAdapter_Confirm.DataHolder data){
        mViewModel_confirm.deleteFile(data.id);
        for(int i=0; i < fileList_confirm.size(); i++){
            if(fileList_confirm.get(i).getId().equals(data.id)){
                fileList_confirm.remove(i);
                break;
            }
        }

        for(int i=0; i < fileList_select.size(); i++){
            if(fileList_select.get(i).getId().equals(data.id)){
                fileList_select.remove(i);
                break;
            }
        }
        //Save temp list into mViewModel**
        mViewModel_select.setFileList_Select(fileList_select);
        mViewModel_confirm.setFileList_Confirm(fileList_confirm);
        //Save temp list into mViewModel**
    }

    private void initView() {
        mAdapter_select = new FileAdapter_Select();
        mAdapter_select.setCallbacks(this);

        mAdapter_confirm = new FileAdapter_Confirm();
        mAdapter_confirm.setCallbacks(this);

        mRecycleView_selectionList.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        mRecycleView_selectionList.setHasFixedSize(true);
        mRecycleView_selectionList.setAdapter(mAdapter_select);

        mRecycleView_confirmationList.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        mRecycleView_confirmationList.setHasFixedSize(true);
        mRecycleView_confirmationList.setAdapter(mAdapter_confirm);

        fileNameHelper = new FileNameHelper();
    }

    private void initViewModel() {
        mViewModel_select = new ViewModelProvider(this, new FileViewSelectModelFactory(getActivity().getApplication())).get(FileViewSelectModel.class);
        mViewModel_select.getFileAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<FileAdapter_Select.DataHolder>>() {
            @Override
            public void onChanged(List<FileAdapter_Select.DataHolder> dataHolders) {
                if(dataHolders.isEmpty()){
                    textView_SelectEmpty.setVisibility(View.VISIBLE);
                }else{
                    textView_SelectEmpty.setVisibility(View.GONE);
                }
                mAdapter_select.setData(dataHolders);
                mAdapter_select.notifyDataSetChanged();

            }
        });

        mViewModel_confirm = new ViewModelProvider(this, new FileViewConfirmModelFactory(getActivity().getApplication())).get(FileViewConfirmModel.class);
        mViewModel_confirm.getFileAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<FileAdapter_Confirm.DataHolder>>() {

            @Override
            public void onChanged(List<FileAdapter_Confirm.DataHolder> dataHolders) {
                if(dataHolders.isEmpty()){
                    textview_ConfirmEmpty.setVisibility(View.VISIBLE);
                }else{
                    textview_ConfirmEmpty.setVisibility(View.GONE);
                }
                mAdapter_confirm.setData(dataHolders);
                mAdapter_confirm.notifyDataSetChanged();

            }
        });

        if(mViewModel_select.getFileList_Select() == null && fileList_select == null){
            fileList_select = new ArrayList<>();
            mViewModel_select.setFileList_Select(fileList_select);
        }
        else{
            fileList_select = mViewModel_select.getFileList_Select();
        }
        if(mViewModel_confirm.getFileList_Confirm() == null && fileList_confirm == null){
            fileList_confirm = new ArrayList<>();
            mViewModel_confirm.setFileList_Confirm(fileList_confirm);
        }else{
            fileList_confirm = mViewModel_confirm.getFileList_Confirm();
        }
    }

    @OnClick(R.id.button_select_files)
    protected void selectFilesForRename(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent.createChooser(intent, "Choose one or many file."), OPEN_FILES_REQUEST_CODE);
    }

    @OnClick(R.id.button_select_folder)
    protected void selectFoldersForRename(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        startActivityForResult(intent.createChooser(intent, "Choose one folder."), OPEN_FOLDER_REQUEST_CODE);
    }

    @OnClick(R.id.button_reset)
    protected void resetSelectConfirmList(){
        fileList_select.clear();
        fileList_confirm.clear();
        mViewModel_select.loadFileAdapterList(fileList_select);
        mViewModel_confirm.loadFileAdapterList(fileList_confirm);
    }

    @OnClick(R.id.button_next)
    protected void toRenameCriteria(){
        if(fileList_confirm.size() == 0){
            new MaterialDialog.Builder(getActivity())
                    .title("Empty Confirmation List")
                    .content("** Please select files into the confirmation list before proceeding.**")
                    .contentColorRes(R.color.colorRed)
                    .positiveText("Ok")
                    .show();
        }else{
            mCallbacks.onProceedToRenameTaskFragment();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultCode == Activity.RESULT_OK) {
            String fileName = "";
            String extension = "";
            if (requestCode == OPEN_FILES_REQUEST_CODE) {
                if (resultData != null) {
                    //Single Files Selection **
                    if(resultData.getClipData() == null){
                        DocumentFile documentFile = DocumentFile.fromSingleUri(getActivity().getApplication(), resultData.getData());
                        //Check for first time selecting files
                        if(fileList_select.isEmpty()){
                            fileList_select.add(new file(DocumentsContract.getDocumentId(documentFile.getUri()),
                                    documentFile.getUri(),
                                    fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFile.getUri()).get(2),
                                    documentFile.getUri().getPath(),
                                    fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFile.getUri()).get(3),
                                    documentFile.length()));
                            mViewModel_select.loadFileAdapterList(fileList_select);
                            //Save temp list into mViewModel**
                            mViewModel_select.setFileList_Select(fileList_select);
                            //Save temp list into mViewModel**
                        }else{
                            Boolean similarFlag = false;
                            Boolean changesFlag = false;
                            //Check for duplication before inserting into adapterlist
                            for(int i=0; i < fileList_select.size(); i++){
                                if(fileList_select.get(i).getId().equals(DocumentsContract.getDocumentId(documentFile.getUri()))){
                                    similarFlag = true;
                                    break;
                                }
                            }
                            if(similarFlag == false){
                                fileList_select.add(new file(DocumentsContract.getDocumentId(documentFile.getUri()),
                                        documentFile.getUri(),
                                        fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFile.getUri()).get(2),
                                        documentFile.getUri().getPath(),
                                        fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFile.getUri()).get(3),
                                        documentFile.length()));
                                changesFlag = true;
                            }
                            if(changesFlag){
                                mViewModel_select.loadFileAdapterList(fileList_select);
                                //Save temp list into mViewModel**
                                mViewModel_select.setFileList_Select(fileList_select);
                                //Save temp list into mViewModel**
                            }
                        }
                    }
                    //Single Files Selection **

                    // Multiple Files Selection **
                    else{
                        ClipData clipData = resultData.getClipData();
                        if(fileList_select.isEmpty()){
                            for(int i=0; i < clipData.getItemCount(); i++){
                                DocumentFile documentFile = DocumentFile.fromSingleUri(getActivity().getApplication(), resultData.getClipData().getItemAt(i).getUri());
                                fileList_select.add(new file(DocumentsContract.getDocumentId(documentFile.getUri()),
                                        documentFile.getUri(),
                                        fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFile.getUri()).get(2),
                                        documentFile.getUri().getPath(),
                                        fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFile.getUri()).get(3),
                                        documentFile.length()));
                            }
                            mViewModel_select.loadFileAdapterList(fileList_select);
                            //Save temp list into mViewModel**
                            mViewModel_select.setFileList_Select(fileList_select);
                            //Save temp list into mViewModel**
                        }else{
                            Boolean similarFlag;
                            Boolean changesFlag = false;
                            int x = 0;
                             do {
                                 similarFlag = false;
                                 DocumentFile documentFile = DocumentFile.fromSingleUri(getActivity().getApplication(), resultData.getClipData().getItemAt(x).getUri());
                                 for (int i = 0 ; i < fileList_select.size(); i++) {
                                     if(fileList_select.get(i).getId().equals(DocumentsContract.getDocumentId(documentFile.getUri()))){
                                        similarFlag = true;
                                        break;
                                     }
                                 }
                                 if(similarFlag == false){
                                     fileList_select.add(new file(DocumentsContract.getDocumentId(documentFile.getUri()),
                                             documentFile.getUri(),
                                             fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFile.getUri()).get(2),
                                             documentFile.getUri().getPath(),
                                             fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFile.getUri()).get(3),
                                             documentFile.length()));
                                     changesFlag = true;
                                 }
                                 x++;
                             }while(x < resultData.getClipData().getItemCount());
                             if(changesFlag){
                                 mViewModel_select.loadFileAdapterList(fileList_select);
                                 //Save temp list into mViewModel**
                                 mViewModel_select.setFileList_Select(fileList_select);
                                 //Save temp list into mViewModel**
                             }
                        }

                    }
                    // Multiple Files Selection **
                }
            }
            //Directory Folder Selection **
            else if (requestCode == OPEN_FOLDER_REQUEST_CODE){
                DocumentFile documentFile = DocumentFile.fromTreeUri(getActivity().getApplication(), resultData.getData());

                DocumentFile[] documentFiles = documentFile.listFiles();

                if(fileList_select.isEmpty()){
                    for (int i = 0; i < documentFile.listFiles().length; i++) {
                        //Check Duplication before loading into Adapterlist
                        fileList_select.add(new file(DocumentsContract.getDocumentId(documentFiles[i].getUri()),
                                documentFiles[i].getUri(),
                                fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFiles[i].getUri()).get(2),
                                documentFiles[i].getUri().getPath(),
                                fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFiles[i].getUri()).get(3),
                                documentFiles[i].length()));
                    }
                    mViewModel_select.loadFileAdapterList(fileList_select);
                    //Save temp list into mViewModel**
                    mViewModel_select.setFileList_Select(fileList_select);
                    //Save temp list into mViewModel**
                }else{
                    int x = 0;
                    Boolean similarFlag = false;
                    Boolean changesFlag = false;
                    do{
                        similarFlag = false;
                        for(int i=0; i < fileList_select.size(); i++){
                            if(fileList_select.get(i).getId().equals(DocumentsContract.getDocumentId(documentFiles[x].getUri()))){
                                similarFlag = true;
                                break;
                            }
                        }
                        if(similarFlag == false){
                            fileList_select.add(new file(DocumentsContract.getDocumentId(documentFiles[x].getUri()),
                                    documentFiles[x].getUri(),
                                    fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFiles[x].getUri()).get(2),
                                    documentFiles[x].getUri().getPath(),
                                    fileNameHelper.getFilesDetailsFromUri(getActivity().getApplication(), documentFiles[x].getUri()).get(3),
                                    documentFiles[x].length()));
                            changesFlag = true;
                        }
                        x++;
                    }while(x < documentFiles.length);

                    if(changesFlag){
                        mViewModel_select.loadFileAdapterList(fileList_select);
                        //Save temp list into mViewModel**
                        mViewModel_select.setFileList_Select(fileList_select);
                        //Save temp list into mViewModel**
                    }
                }
            }
            //Directory Folder Selection **
        }
    }

    public interface Callbacks{
        void onProceedToRenameTaskFragment();
    }
}
