package com.example.rename.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.rename.R;
import com.example.rename.adapters.TaskAdapter;
import com.example.rename.entitiesModel.entities.InsertChar;
import com.example.rename.entitiesModel.entities.InsertFileSize;
import com.example.rename.entitiesModel.entities.InsertNumber;
import com.example.rename.entitiesModel.entities.NewName;
import com.example.rename.entitiesModel.entities.ReplaceChars;
import com.example.rename.entitiesModel.entities.Task;
import com.example.rename.repository.TaskRepository;
import com.example.rename.utils.DatabaseHelper;
import com.example.rename.viewModels.PreviewViewModel.PreviewViewModelFactory;
import com.example.rename.viewModels.TaskViewModel.TaskViewModel;
import com.example.rename.viewModels.TaskViewModel.TaskViewModelFactory;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RenameTaskFragment extends Fragment implements TaskAdapter.Callbacks {
    public static final String TAG = com.example.rename.fragments.RenameTaskFragment.class.getSimpleName();

    @BindView(R.id.recycleView_taskList)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.textView_emptyCriteriaList)
    protected TextView textview_emptyCriteriaList;

    @BindView(R.id.edittext_task_title)
    protected EditText edittext_task_title;

    private Unbinder mUnbinder;
    private TaskAdapter mAdapter;
    private TaskViewModel mViewModel;

    private String[] dialogAddTaskListArray = new String[5];
    private String[] dialogInsertCharListArray = new String[3];
    private String[] dialogFileSizeFormatArray = new String[3];
    private List<String> dialogImportTaskList;

    private MaterialDialog dialogAddTask, dialogInsertPosition, dialogRenameCriteriaInput, dialogInsertCustomPosition,
                            dialogSelectFileSize, dialogReplaceCharacters, dialogDeleteConfirmation;

    private Task tempNewTask;

    private Callbacks mCallbacks;

    private Boolean emptyFlag = false;

    private List<TaskAdapter.DataHolder> dataHoldersDialog = new ArrayList<>();

    public RenameTaskFragment(){
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static com.example.rename.fragments.RenameTaskFragment newInstance() {
        return new com.example.rename.fragments.RenameTaskFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_rename_task, container, false);

        mUnbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        tempNewTask = mViewModel.getTempTaskLiveData().getValue();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onDeleteButtonClicked(TaskAdapter.DataHolder data) {
        mViewModel.deleteCriteria(data.id);
    }

    private void initView() {
        mAdapter = new TaskAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        //Add New task dialog **
        dialogAddTaskListArray[0] = "Insert Characters";
        dialogAddTaskListArray[1] = "Insert File Size";
        dialogAddTaskListArray[2] = "Insert Numbers";
        dialogAddTaskListArray[3] = "New Name";
        dialogAddTaskListArray[4] = "Replace Characters";

        dialogInsertCharListArray[0] = "Begin Place of file name";
        dialogInsertCharListArray[1] = "Last Place of file name";
        dialogInsertCharListArray[2] = "Custom Index Inserting";

        dialogFileSizeFormatArray[0] = ".MB";
        dialogFileSizeFormatArray[1] = ".KB";
        dialogFileSizeFormatArray[2] = "Bytes";
        //Add New task dialog **
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new TaskViewModelFactory(getActivity().getApplication())).get(TaskViewModel.class);
        mViewModel.getTaskAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<TaskAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<TaskAdapter.DataHolder> dataHolders) {

                if(dataHolders.isEmpty()){
                    textview_emptyCriteriaList.setVisibility(View.VISIBLE);
                }else{
                    textview_emptyCriteriaList.setVisibility(View.GONE);
                }

                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();
            }
        });

        mViewModel.getTaskAdapterListDialogLiveData().observe(getViewLifecycleOwner(), new Observer<List<TaskAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<TaskAdapter.DataHolder> dataHolders) {
                dataHoldersDialog = dataHolders;
            }
        });

        mViewModel.loadTaskAdapterListDialogLiveData();
        edittext_task_title.setText(mViewModel.getTempTaskTitle());
    }

    @OnClick(R.id.button_add_rename_task)
    protected void addNewRenameTask(){
        if(tempNewTask == null){
            tempNewTask = new Task("");
        }
        //Declaration for mass reuse for dialogRenameCriteria **
        dialogRenameCriteriaInput = new MaterialDialog.Builder(getActivity())
                .title("New Name for Rename")
                .content(Html.fromHtml("<b>Enter your new name without " + Html.fromHtml("&lt; &gt; &#34; ? / | &#92; : *") + ".</b>"))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .inputRangeRes(1, 8, R.color.colorRed)
                .input("Enter the input characters.", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        try{
                            String inputString = "";
                            for(int i=0; i < input.length(); i++) {
                                inputString += input.charAt(i);
                            }
                            if(inputString.matches(".*[\\\\?<>*:|/]*")){
                                throw new Exception("Tak Halal File Name.");
                            }

                        }catch(Exception e){
                            dialog.getInputEditText().setError("A filename can't contain any of the following characters:" +
                                    System.lineSeparator() + Html.fromHtml("&lt; &gt; &#34; ? / | &#92; : *"));
                        }
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        dialog.cancel();
                        //Insert Characters **
                        if(dialogAddTask.getSelectedIndex() == 0){
                            InsertChar insertChar = new InsertChar();
                            //Insert Beginning **
                            if(dialogInsertPosition.getSelectedIndex() == 0){

                                insertChar.setTitle("Insert " + dialog.getInputEditText().getText() + " " +
                                        "into filename's begin.");
                                insertChar.setInsertCharacter(dialog.getInputEditText().getText().toString());
                                insertChar.setInsertIndex(0);
                            }
                            //Insert Beginning **
                            //Insert Back **
                            else if(dialogInsertPosition.getSelectedIndex() == 1){
                                insertChar.setTitle("Insert " + dialog.getInputEditText().getText() + " " +
                                        "into filename's back.");
                                insertChar.setInsertCharacter(dialog.getInputEditText().getText().toString());
                                insertChar.setInsertIndex(-1);
                            }
                            //Insert Back **
                            //Insert Custom **
                            else{
                                insertChar.setTitle("Insert " + dialog.getInputEditText().getText() + " " +
                                        "into filename at the Index of " + dialogInsertCustomPosition.getInputEditText().getText().toString());
                                insertChar.setInsertCharacter(dialog.getInputEditText().getText().toString());
                                insertChar.setInsertIndex(Integer.parseInt(dialogInsertCustomPosition.getInputEditText().getText().toString()));
                            }
                            //Insert Custom **
                            tempNewTask.getInsertCharNoDb().add(insertChar);
                            mViewModel.addTempCriteriaToLiveData(null, null,null,
                                    null, insertChar);
                        }
                        //Insert Characters **
                        //Insert Numbers **
                        else if(dialogAddTask.getSelectedIndex() == 2){
                            InsertNumber insertNumber = new InsertNumber();
                            //Insert Beginning **
                            if(dialogInsertPosition.getSelectedIndex() == 0){

                                insertNumber.setTitle("Insert " + dialog.getInputEditText().getText() + " " +
                                        "into filename's begin.");
                                insertNumber.setInsertNumbers(Double.parseDouble(dialog.getInputEditText().getText().toString()));
                                insertNumber.setInsertIndex(0);
                            }
                            //Insert Beginning **
                            //Insert Back **
                            else if(dialogInsertPosition.getSelectedIndex() == 1){
                                insertNumber.setTitle("Insert " + dialog.getInputEditText().getText() + " " +
                                        "into filename's back.");
                                insertNumber.setInsertNumbers(Double.parseDouble(dialog.getInputEditText().getText().toString()));
                                insertNumber.setInsertIndex(-1);
                            }
                            //Insert Back **
                            //Insert Custom **
                            else{
                                insertNumber.setTitle("Insert " + dialog.getInputEditText().getText() + " " +
                                        "into filename at the Index of " + dialogInsertCustomPosition.getInputEditText().getText().toString());
                                insertNumber.setInsertNumbers(Double.parseDouble(dialog.getInputEditText().getText().toString()));
                                insertNumber.setInsertIndex(Integer.parseInt(dialogInsertCustomPosition.getInputEditText().getText().toString()));
                            }
                            //Insert Custom **

                            tempNewTask.getInsertNumberNoDb().add(insertNumber);
                            mViewModel.addTempCriteriaToLiveData(null, null,insertNumber,
                                    null, null);
                        }
                        //Insert Numbers **
                        //New Name **
                        else if(dialogAddTask.getSelectedIndex() == 3){
                            NewName newName = new NewName();
                            //Insert New Name **
                            newName.setTitle("Rename file with " + dialog.getInputEditText().getText() + ".");
                            newName.setNewName(dialog.getInputEditText().getText().toString());
                            //Insert New Name **

                            tempNewTask.getNewNameNoDb().add(newName);
                            mViewModel.addTempCriteriaToLiveData(null, newName, null,
                                    null, null);
                        }
                        //New Name **
                        //Replace Characters **
                        else if(dialogAddTask.getSelectedIndex() == 4){
                            ReplaceChars replaceChars = new ReplaceChars();
                            replaceChars.setTitle("Replace Filename characters with " + dialog.getInputEditText().getText().toString() +
                                    " if " + dialogReplaceCharacters.getInputEditText().getText().toString() +" is in filename.");
                            replaceChars.setOldCharacter(dialogReplaceCharacters.getInputEditText().getText().toString());
                            replaceChars.setReplaceCharacter(dialog.getInputEditText().getText().toString());

                            tempNewTask.getReplaceCharsNoDB().add(replaceChars);
                            mViewModel.addTempCriteriaToLiveData(replaceChars, null, null, null,
                                    null);
                        }
                        //Replace Characters **

                        dialog.dismiss();
                        dialog.cancel();
                    }
                })
                .negativeText("Back")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.cancel();
                        dialog.dismiss();

                        dialogAddTask.show();
                    }
                })
                .autoDismiss(false)
                .build();
        //Declaration for mass reuse for dialogRenameCriteria **

        //Main Dialog for Adding new task criteria **
        dialogAddTask = new MaterialDialog.Builder(getActivity())
                .title("Add New Rename Task")
                .content(Html.fromHtml("<b>Choose the type of rename criteria.</b>"))
                .items(dialogAddTaskListArray)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return true;
                    }
                })
                .autoDismiss(false)
                .positiveText("Proceed")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();

                        if(dialog.getSelectedIndex() == 3){
                            dialogRenameCriteriaInput.show();
                        }else if(dialog.getSelectedIndex() == 4){
                            dialogReplaceCharacters = new MaterialDialog.Builder(getActivity())
                                    .title("Replace Characters in filename for Rename")
                                    .content(Html.fromHtml("<b>Enter your replace characters without " + Html.fromHtml("&lt; &gt; &#34; ? / | &#92; : *" + ".</b>")) + System.lineSeparator() +
                                            "The entered characters will be automatically matched cased for replace.")
                                    .inputType(InputType.TYPE_CLASS_TEXT)
                                    .inputRangeRes(1, 8, R.color.colorRed)
                                    .input("Enter the input replace characters here.", "", new MaterialDialog.InputCallback() {
                                        @Override
                                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                            try{
                                                String inputString = "";
                                                for(int i=0; i < input.length(); i++) {
                                                    inputString += input.charAt(i);
                                                }
                                                if(inputString.matches(".*[\\\\?<>*:|/]*")){
                                                    throw new Exception("Tak Halal File Name.");
                                                }

                                            }catch(Exception e){
                                                dialog.getInputEditText().setError("A filename can't contain any of the following characters:" +
                                                        System.lineSeparator() + Html.fromHtml("&lt; &gt; &#34; ? / | &#92; : *"));
                                            }
                                        }
                                    })
                                    .positiveText("Proceed")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.dismiss();

                                            dialogRenameCriteriaInput.show();
                                        }
                                    })
                                    .negativeText("Back")
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.cancel();
                                            dialog.dismiss();
                                            dialogAddTask.show();
                                        }
                                    })
                                    .autoDismiss(false)
                                    .show();
                        }
                        else{
                            //Insert Characters **
                            dialogInsertPosition = new MaterialDialog.Builder(getActivity())
                                    .title("Insert Position into File Name")
                                    .content(Html.fromHtml("<b>Choose the position of insertion.</b>"))
                                    .items(dialogInsertCharListArray)
                                    .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                                        @Override
                                        public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                            return true;
                                        }
                                    })
                                    .positiveText("Proceed")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.dismiss();
                                            if(dialogAddTask.getSelectedIndex() == 0){
                                                dialogRenameCriteriaInput.setTitle("Insert Characters for Rename");
                                                dialogRenameCriteriaInput.setContent(Html.fromHtml("<b>Enter your input characters without " + Html.fromHtml("&lt; &gt; &#34; ? / | &#92; : *") + ".</b>"));
                                            }
                                            //Insert File Size **
                                            else if(dialogAddTask.getSelectedIndex() == 1){
                                                dialogSelectFileSize = new MaterialDialog.Builder(getActivity())
                                                        .title("Select the file size format for Rename.")
                                                        .items(dialogFileSizeFormatArray)
                                                        .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                                                            @Override
                                                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                                                return true;
                                                            }
                                                        })
                                                        .positiveText("Proceed")
                                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                            @Override
                                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                InsertFileSize insertFileSize = new InsertFileSize();

                                                                String filePosition = "";
                                                                String fileSizeFormat = "";
                                                                //.MB File Size Format **
                                                                if(dialog.getSelectedIndex() == 0){
                                                                    insertFileSize.setUnit(".MB");
                                                                    fileSizeFormat = ".MB";
                                                                }  //.MB File Size Format **
                                                                //.KB File Size Format **
                                                                else if(dialog.getSelectedIndex() == 1) {
                                                                    insertFileSize.setUnit(".KB");
                                                                    fileSizeFormat = ".KB";
                                                                } //.KB File Size Format **
                                                                //Bytes File Size Format **
                                                                else {
                                                                    insertFileSize.setUnit(".Bytes");
                                                                    fileSizeFormat = ".Bytes";
                                                                }//Bytes File Size Format **

                                                                //Insert Begin **
                                                                if(dialogInsertPosition.getSelectedIndex() == 0) {
                                                                    insertFileSize.setInsertIndex(0);
                                                                    filePosition = "begin";
                                                                }//Insert Begin **
                                                                //Insert Back **
                                                                else if (dialogInsertPosition.getSelectedIndex() == 1) {
                                                                    insertFileSize.setInsertIndex(-1);
                                                                    filePosition = "back";
                                                                }//Insert Back **
                                                                //Insert Custom **
                                                                else {
                                                                    insertFileSize.setInsertIndex(Integer.parseInt(dialogInsertCustomPosition.getInputEditText().getText().toString()));
                                                                    filePosition = "index of " + dialogInsertCustomPosition.getInputEditText().getText().toString();
                                                                }//Insert Custom **

                                                                insertFileSize.setTitle("Insert File Size with format of " + fileSizeFormat + " in filename's " + filePosition +".");
                                                                tempNewTask.getInsertFileSizeNoDb().add(insertFileSize);
                                                                mViewModel.addTempCriteriaToLiveData(null, null, null,
                                                                        insertFileSize, null);

                                                                dialog.cancel();
                                                                dialog.dismiss();
                                                            }
                                                        })
                                                        .negativeText("Back")
                                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                            @Override
                                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                dialog.cancel();
                                                                dialog.dismiss();
                                                                dialogInsertPosition.show();
                                                            }
                                                        })
                                                        .autoDismiss(false)
                                                        .build();
                                            }//Insert File Size **
                                            //Insert Numbers **
                                            else if(dialogAddTask.getSelectedIndex() == 2){
                                                dialogRenameCriteriaInput.setTitle("Insert Numbers for Rename");
                                                dialogRenameCriteriaInput.setContent(Html.fromHtml("<b>Enter your input numbers without " + Html.fromHtml("&lt; &gt; &#34; ? / | &#92; : *") + ".</b>"));
                                                dialogRenameCriteriaInput.getInputEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                                            }//Insert Numbers **

                                            if(dialog.getSelectedIndex() > 1){
                                                dialogInsertCustomPosition = new MaterialDialog.Builder(getActivity())
                                                        .title("Insert Custom Position for Rename")
                                                        .content(Html.fromHtml("<b>Enter the Index where the starting number is 0."))
                                                        .inputType(InputType.TYPE_CLASS_NUMBER)
                                                        .inputRangeRes(1, 4, R.color.colorRed)
                                                        .input("Enter 1 input characters.", "", new MaterialDialog.InputCallback() {
                                                            @Override
                                                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                                            }
                                                        })
                                                        .positiveText("Proceed")
                                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                            @Override
                                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                dialog.dismiss();

                                                                //To Dialog Select File Size **
                                                                if(dialogAddTask.getSelectedIndex() == 1){
                                                                    dialogSelectFileSize.show();
                                                                }//To Dialog Select File Size **
                                                                //To Dialog Replace Characters **
                                                                else if(dialogAddTask.getSelectedIndex() == 4){
                                                                    dialogReplaceCharacters.show();
                                                                }//To Dialog Replace Characters **
                                                                //To Dialog Rename Criteria Input **
                                                                else{
                                                                    dialogRenameCriteriaInput.show();
                                                                }//To Dialog Rename Criteria Input **
                                                            }
                                                        })
                                                        .negativeText("Cancel")
                                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                            @Override
                                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                dialog.cancel();
                                                                dialog.dismiss();
                                                                dialogInsertPosition.show();
                                                            }
                                                        }).show();
                                            }else{
                                                //To Dialog Select File Size with Skipping Custom Indexing **
                                                if(dialogAddTask.getSelectedIndex() == 1){
                                                    dialogSelectFileSize.show();
                                                }//To Dialog Select File Size with Skipping Custom Indexing **
                                                else if (dialogAddTask.getSelectedIndex() == 4){
                                                    //Dummy Functionality
                                                }//To Dialog Rename Criteria Input with Skipping Custom Indexing **
                                                else{
                                                    dialogRenameCriteriaInput.show();
                                                }//To Dialog Rename Criteria Input with Skipping Custom Indexing **
                                            }
                                        }
                                    })
                                    .negativeText("Back")
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.cancel();
                                            dialog.dismiss();
                                            dialogAddTask.show();
                                        }
                                    })
                                    .autoDismiss(false)
                                    .show();
                        }
                    }
                })
                .show();
        //Main Dialog for Adding new task criteria **
    }

    @OnClick(R.id.button_task_next)
    public void proceedToPreview(){
        if(mViewModel.getTaskAdapterListLiveData().getValue() == null || mViewModel.getTaskAdapterListLiveData().getValue().size() == 0){
            new MaterialDialog.Builder(getActivity())
                    .title("Empty Task Criteria List")
                    .content("Please add task criteria's before proceeding.")
                    .contentColorRes(R.color.colorRed)
                    .positiveText("Ok")
                    .show();
        }else{
            new MaterialDialog.Builder(getActivity())
                    .title("Save Task Confirmation")
                    .content("Save before proceed?")
                    .positiveText("Yes")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if(!edittext_task_title.getText().toString().isEmpty()){
                                Boolean similarTaskTitleFlag = false;
                                for(int i=0; i < dataHoldersDialog.size();i++){
                                    if(edittext_task_title.getText().toString().equals(dataHoldersDialog.get(i).title)){
                                        similarTaskTitleFlag = true;
                                        break;
                                    }
                                }

                                if(!similarTaskTitleFlag){
                                    mViewModel.setTempTaskTitle(edittext_task_title.getText().toString());
                                    mViewModel.newTask(edittext_task_title.getText().toString(), tempNewTask);
                                    mCallbacks.onProceedToPreviewFragment();
                                }else{
                                    new MaterialDialog.Builder(getActivity())
                                            .title("Similar Task Title")
                                            .content("Similar task title existed, please use other task title to save.")
                                            .contentColorRes(R.color.colorRed)
                                            .positiveText("OK")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    dialog.dismiss();
                                                    dialog.cancel();
                                                }
                                            })
                                            .autoDismiss(false)
                                            .show();
                                }
                            }else{
                                new MaterialDialog.Builder(getActivity())
                                        .title("Empty Title")
                                        .content("Please enter a title name for the task to proceed to the next stage.")
                                        .contentColorRes(R.color.colorRed)
                                        .positiveText("Ok.")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                dialog.cancel();
                                                dialog.dismiss();
                                            }
                                        }).show();
                            }
                            dialog.dismiss();
                            dialog.cancel();
                        }
                    })
                    .negativeText("No")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            dialog.cancel();
                            mViewModel.setTempTaskTitle(edittext_task_title.getText().toString());
                            mViewModel.setTempTaskLiveData(tempNewTask);
                            mCallbacks.onProceedToPreviewFragment();
                        }
                    })
                    .autoDismiss(false)
                    .show();
        }
    }

    public void initializeDialogImportTaskList(){
        dialogImportTaskList = new ArrayList<>();
        if(dataHoldersDialog.size() == 0){
            dialogImportTaskList.add("There is no task saved.");
            emptyFlag = true;
        }else{
            emptyFlag = false;

            for(int y=0; y < dataHoldersDialog.size(); y++){
                dialogImportTaskList.add(dataHoldersDialog.get(y).title);
            }
        }
    }

    @OnClick(R.id.button_save_rename_task)
    public void importSavedTaskFromDB(){
        initializeDialogImportTaskList();

        dialogDeleteConfirmation = new MaterialDialog.Builder(getActivity())
                            .title("Saved Task History List")
                            .items(dialogImportTaskList)
                            .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                                @Override
                                public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                    return true;
                                }
                            })
                            .neutralText("Delete")
                            .onNeutral(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    new MaterialDialog.Builder(getActivity())
                                            .title("Confirmation")
                                            .content("Confirm delete?")
                                            .positiveText("Yes")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    mViewModel.deleteTask(dataHoldersDialog.get(dialogDeleteConfirmation.getSelectedIndex()).taskID);
                                                    dialogDeleteConfirmation.cancel();
                                                    dialog.dismiss();
                                                    dialog.cancel();
                                                }
                                            })
                                            .negativeText("No")
                                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    dialog.dismiss();
                                                    dialog.cancel();
                                                    dialogDeleteConfirmation.show();
                                                }
                                            }).autoDismiss(false)
                                            .show();
                                }
                            })
                            .negativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    dialog.cancel();
                                }
                            })
                            .positiveText("Import")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if(!emptyFlag){
                                        tempNewTask = mViewModel.loadTaskAdapterByIdList(dataHoldersDialog.get(dialog.getSelectedIndex()).taskID);
                                        edittext_task_title.setText(dataHoldersDialog.get(dialog.getSelectedIndex()).title);
                                    }
                                    dialog.dismiss();
                                    dialog.cancel();
                                }
                            })
                            .autoDismiss(false)
                            .show();
    }

    @OnClick(R.id.button_task_reset)
    public void resetInputRenameTaskFragment(){
        mViewModel.resetAllVariables();
        edittext_task_title.setText("");
    }

    public interface Callbacks{
        void onProceedToPreviewFragment();
    }
}
