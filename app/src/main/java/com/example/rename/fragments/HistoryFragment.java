package com.example.rename.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.rename.R;
import com.example.rename.adapters.HistoryAdapter;
import com.example.rename.adapters.PreviewHistoryAdapter;
import com.example.rename.entitiesModel.entities.RenameHistory;
import com.example.rename.viewModels.HistoryViewModel.HistoryViewModel;
import com.example.rename.viewModels.HistoryViewModel.HistoryViewModelFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HistoryFragment extends Fragment implements HistoryAdapter.Callbacks{
    @BindView(R.id.recycleView_historyList)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.textView_HistoryEmpty)
    protected TextView textView_HistoryEmpty;

    private Unbinder mUnbinder;
    private HistoryAdapter mAdapter;
    private PreviewHistoryAdapter mAdapterPreviewHistory;
    private HistoryViewModel mViewModel;

    public static final String TAG = HistoryFragment.class.getSimpleName();

    public HistoryFragment(){
    }

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_history, container, false);

        mUnbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    private void initView() {
        mAdapter = new HistoryAdapter();
        mAdapter.setCallbacks(this);

        mAdapterPreviewHistory = new PreviewHistoryAdapter();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new HistoryViewModelFactory(getActivity().getApplication())).get(HistoryViewModel.class);
        mViewModel.getHistoryAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<HistoryAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<HistoryAdapter.DataHolder> dataHolders) {

                if(dataHolders.isEmpty()){
                    textView_HistoryEmpty.setVisibility(View.VISIBLE);
                }else{
                    textView_HistoryEmpty.setVisibility(View.GONE);
                }

                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

            }
        });

        mViewModel.getPreviewHistoryListLiveData().observe(getViewLifecycleOwner(), new Observer<List<PreviewHistoryAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<PreviewHistoryAdapter.DataHolder> dataHolders) {
                mAdapterPreviewHistory.setData(dataHolders);
                mAdapterPreviewHistory.notifyDataSetChanged();
            }
        });

        // Load data into adapter.
        mViewModel.loadHistoryAdapterList();
    }

    @Override
    public void onListItemClicked(HistoryAdapter.DataHolder data) {
        MaterialDialog previewHistoryDialog = new MaterialDialog.Builder(getActivity())
                                            .title("Rename Task Details")
                                            .customView(R.layout.preview_history_custom_dialog, false)
                                            .positiveText("Ok")
                                            .build();

        RecyclerView recyclerView = previewHistoryDialog.getView().findViewById(R.id.recycleView_previewHistoryList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapterPreviewHistory);

        mViewModel.loadSingleHistory(data.id);

        previewHistoryDialog.show();
    }
}
