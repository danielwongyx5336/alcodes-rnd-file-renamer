package com.example.rename.fragments;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.rename.BuildConfig;
import com.example.rename.R;
import com.example.rename.adapters.FileAdapter_Confirm;
import com.example.rename.adapters.PreviewAdapter;
import com.example.rename.adapters.TaskAdapter;
import com.example.rename.entitiesModel.entities.file;
import com.example.rename.entitiesModel.entities.Task;
import com.example.rename.utils.FileNameHelper;
import com.example.rename.viewModels.FileViewConfirmModel.FileViewConfirmModel;
import com.example.rename.viewModels.FileViewConfirmModel.FileViewConfirmModelFactory;
import com.example.rename.viewModels.PreviewViewModel.PreviewViewModel;
import com.example.rename.viewModels.PreviewViewModel.PreviewViewModelFactory;
import com.example.rename.viewModels.TaskViewModel.TaskViewModel;
import com.example.rename.viewModels.TaskViewModel.TaskViewModelFactory;
import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PreviewFragment extends Fragment {
    public static final String TAG = com.example.rename.fragments.PreviewFragment.class.getSimpleName();

    @BindView(R.id.recycleView_previewList)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.textView_PreviewEmpty)
    protected TextView textView_previewEmpty;

    private Unbinder mUnbinder;

    private PreviewAdapter mAdapter;
    private PreviewViewModel mViewModel;
    private FileViewConfirmModel mViewModel_FileConfirmList;
    private TaskViewModel mViewModel_TaskList;

    private Callbacks mCallbacks;

    private static final int OPEN_FOLDER_REQUEST_CODE_CHOOSER = 43;

    public static com.example.rename.fragments.PreviewFragment newInstance() {
        return new com.example.rename.fragments.PreviewFragment();
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_preview, container, false);

        mUnbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        if (BuildConfig.DEBUG) {
//            // Auto fill in credential for testing purpose.
//        }

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    private void initView() {
        mAdapter = new PreviewAdapter();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new PreviewViewModelFactory(getActivity().getApplication())).get(PreviewViewModel.class);
        mViewModel.getFileAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<PreviewAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<PreviewAdapter.DataHolder> dataHolders) {
                if(dataHolders.isEmpty()){
                    textView_previewEmpty.setVisibility(View.VISIBLE);
                    mRecyclerView.setMinimumHeight(565);
                }else{
                    textView_previewEmpty.setVisibility(View.GONE);
                    mRecyclerView.setMinimumHeight(525);
                }

                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();
            }
        });

        mViewModel_FileConfirmList = new ViewModelProvider(this, new FileViewConfirmModelFactory(getActivity().getApplication())).get(FileViewConfirmModel.class);

        mViewModel_TaskList = new ViewModelProvider(this, new TaskViewModelFactory(getActivity().getApplication())).get(TaskViewModel.class);

        mViewModel.loadPreviewAdapterList(mViewModel_FileConfirmList.getmFileSelectionConfirmationList().getValue(), mViewModel_TaskList.getTempTaskLiveData().getValue());
    }

    @OnClick(R.id.button_preview_next)
    protected void startToRename(){
        new MaterialDialog.Builder(getActivity())
                .title("Confirmation")
                .content("Confirm to proceed to make rename changes?")
                .positiveText("Yes")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Boolean success = mViewModel.renameProcess();
                        dialog.dismiss();
                        dialog.cancel();

                        if(success){
                            new MaterialDialog.Builder(getActivity())
                                    .title("Success")
                                    .content("Bulk Rename Task is successful.")
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.dismiss();
                                            dialog.cancel();
                                            new MaterialDialog.Builder(getActivity())
                                                    .title("Re-position renamed filename")
                                                    .content("Do you want to output your rename files in other folder?")
                                                    .positiveText("Yes")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                                                            startActivityForResult(intent.createChooser(intent, "Choose one folder."), OPEN_FOLDER_REQUEST_CODE_CHOOSER);
                                                            dialog.dismiss();
                                                            dialog.cancel();
                                                        }
                                                    })
                                                    .negativeText("No")
                                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            dialog.dismiss();
                                                            dialog.cancel();
                                                            mCallbacks.onFinishRenameTask();
                                                        }
                                                    })
                                                    .autoDismiss(false)
                                                    .show();
                                        }
                                    })
                                    .autoDismiss(false)
                                    .show();
                        }else{
                            new MaterialDialog.Builder(getActivity())
                                    .title("Failed bulk rename files tasks")
                                    .content("Bulk Renamed Files task is not successful, please try again.")
                                    .contentColorRes(R.color.colorRed)
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            mCallbacks.onFinishRenameTask();
                                            dialog.dismiss();
                                            dialog.cancel();
                                        }
                                    })
                                    .autoDismiss(false)
                                    .show();
                        }
                    }
                })
                .negativeText("No")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        dialog.cancel();
                    }
                })
                .autoDismiss(false)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultCode == Activity.RESULT_OK) {
            //Directory Folder Selection **
            if (requestCode == OPEN_FOLDER_REQUEST_CODE_CHOOSER){
                Boolean success = mViewModel.fileRenamedOutputLocation(resultData.getData());

                if(success){
                    new MaterialDialog.Builder(getActivity())
                            .title("Successfully moved to selected Folder")
                            .content("Bulk Renamed Files moved to selected folder is successful.")
                            .positiveText("Ok")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    mViewModel.setOutputPath(resultData.getData().getPath());
                                    mCallbacks.onFinishRenameTask();
                                    dialog.dismiss();
                                    dialog.cancel();
                                }
                            })
                            .autoDismiss(false)
                            .show();
                }else{
                    new MaterialDialog.Builder(getActivity())
                            .title("Failed to Moved to Selected Folder")
                            .content("Bulk Renamed Files moved to selected folder is not successful.")
                            .contentColorRes(R.color.colorRed)
                            .positiveText("Ok")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    dialog.cancel();
                                }
                            })
                            .autoDismiss(false)
                            .show();
                }
            }
            //Directory Folder Selection **
        }
    }

    @OnClick(R.id.button_preview_back)
    public void backToTaskCriteriaFragment(){
        mCallbacks.backToTaskCriteria();
    }

    public interface Callbacks{
        void onFinishRenameTask();
        void backToTaskCriteria();
    }
}
