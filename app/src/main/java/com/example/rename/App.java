package com.example.rename;

import android.app.Application;

import com.example.rename.BuildConfig;

import timber.log.Timber;

public class App extends Application{

    @Override
    public void onCreate(){
        super.onCreate();

        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }
    }
}
