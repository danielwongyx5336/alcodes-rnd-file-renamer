package com.example.rename.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import androidx.documentfile.provider.DocumentFile;

import com.example.rename.entitiesModel.entities.file;
import com.example.rename.entitiesModel.entities.Task;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class FileNameHelper {

    public FileNameHelper() {
    }

    public List<file> fileMove(Context context, Uri uri, List<file> tempFileSelectedRecords){
        DocumentFile documentFileDestinationTree = DocumentFile.fromTreeUri(context, uri);
        int i=0;
        for(file files : tempFileSelectedRecords){
            try {
                Uri newUri = DocumentsContract.moveDocument(context.getContentResolver(), files.getUri(), files.getUri(), documentFileDestinationTree.getUri());
                tempFileSelectedRecords.get(i).setUri(newUri);
            } catch (Exception e) {
                if(e instanceof FileNotFoundException){
                    e.printStackTrace();
                }else if(e instanceof IllegalStateException){
                    e.printStackTrace();
                }else if(e instanceof  IllegalStateException) {
                    e.printStackTrace();
                }
            }
            i++;
        }
        return tempFileSelectedRecords;
    }

    public Uri fileRenaming(Context context , Uri uri, String changeName, String extension){
        int duplicateCount = 0;
        String duplicateString = "";
        boolean stopFlag = false;
        Uri newUri;
        do {
            if(duplicateCount != 0){
                duplicateString = " (" + duplicateCount + ")";
            }
            
            try {
                newUri = DocumentsContract.renameDocument(context.getContentResolver(), uri, changeName + duplicateString + extension);
                stopFlag = true;
                return newUri;
            } catch (Exception e) {
                if (e instanceof IllegalStateException) {
                    ++duplicateCount;
                }else if(e instanceof IllegalArgumentException){
                    break;
                }
            }
        }while(stopFlag == false);

        return null;
    }

    public String fileSizeBytesConverter(long size, String convertTo) {
        if (size <= 0)
            return "0";

        int digitGroups = 0;
        if(convertTo.equals("MB")){
            digitGroups = 2;
        }else if(convertTo.equals("KB")) {
            digitGroups = 1;
        }

        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + convertTo;
    }

    public ArrayList<String> getFilesDetailsFromUri(Context context, Uri uri) {
        DocumentFile documentFile = DocumentFile.fromSingleUri(context, uri);
        String extensionWithSlash = "";
        if(documentFile.getType() != null){
            extensionWithSlash = documentFile.getType().substring(documentFile.getType().lastIndexOf("/"));
        }else{
            extensionWithSlash = "";
        }

        ArrayList<String> returnString = new ArrayList<String>();

        //Array Indexes 0.,1.,2.,.... for details
        //0. Path without fileName
        returnString.add(uri.getPath());

        //1. Path without fileName
        returnString.add(uri.getPath().substring(0, uri.getPath().lastIndexOf("/")));

        //2. Filename
        if(documentFile.getName().charAt(0) == '.'){
            returnString.add(documentFile.getName());
        }else if(!documentFile.getName().contains(".")){
            returnString.add(documentFile.getName());
        }
        else{
            returnString.add(documentFile.getName().substring(0, documentFile.getName().lastIndexOf(".")));
        }

        //3. File Extension (Filetype)
        if(!extensionWithSlash.equals("")){
            returnString.add("." + extensionWithSlash.substring(1));
        }else{
            returnString.add("");
        }


        //4. File Size in Bytes
        returnString.add(documentFile.length() + ".b");

        return returnString;
    }
}
