package com.example.rename.utils;

import android.content.Context;

import com.example.rename.entitiesModel.DbOpenHelper;
import com.example.rename.entitiesModel.entities.DaoMaster;
import com.example.rename.entitiesModel.entities.DaoSession;


public class DatabaseHelper {

    private static DaoSession mInstance;

    public static DaoSession getInstance(Context context) {
        if (mInstance == null) {
            synchronized (DatabaseHelper.class) {
                DbOpenHelper dbOpenHelper = new DbOpenHelper(context, "rename_app_V2");

                mInstance = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();

            }
        }

        return mInstance;
    }

    public static DaoSession getNewInstance(Context context){
        DbOpenHelper dbOpenHelper = new DbOpenHelper(context, "rename_app_V2");

        mInstance = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();

        return mInstance;
    }

    private DatabaseHelper() {
    }
}
