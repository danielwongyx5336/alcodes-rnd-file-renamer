package com.example.rename.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceHelper {

    private static SharedPreferences mInstance;

    public static SharedPreferences getInstance(Context context) {
        if (mInstance == null) {
            synchronized (SharedPreferenceHelper.class) {
                if (mInstance == null) {
                    mInstance = context.getSharedPreferences("app", Context.MODE_PRIVATE);
                }
            }
        }

        return mInstance;
    }

    private SharedPreferenceHelper() {
    }
}
