package com.example.rename.repository;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.HistoryAdapter;
import com.example.rename.adapters.PreviewHistoryAdapter;
import com.example.rename.entitiesModel.entities.PreviewHistory;
import com.example.rename.entitiesModel.entities.RenameHistory;
import com.example.rename.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class HistoryRepository {
    private static HistoryRepository mInstance;

    private MutableLiveData<List<HistoryAdapter.DataHolder>> mHistoryAdapterListLiveData = new MutableLiveData<>();
    private MutableLiveData<List<PreviewHistoryAdapter.DataHolder>> mPreviewHistoryListLiveData = new MutableLiveData<>();

    public static HistoryRepository getInstance() {
        if (mInstance == null) {
            synchronized (HistoryRepository.class) {
                mInstance = new HistoryRepository();
            }
        }

        return mInstance;
    }

    public HistoryRepository() {
    }

    public MutableLiveData<List<HistoryAdapter.DataHolder>> getHistoryAdapterListLiveData() {
        return mHistoryAdapterListLiveData;
    }

    public void setHistoryAdapterListLiveData(List<HistoryAdapter.DataHolder> mHistoryAdapterListLiveData) {
        this.mHistoryAdapterListLiveData.setValue(mHistoryAdapterListLiveData);
    }

    public MutableLiveData<List<PreviewHistoryAdapter.DataHolder>> getPreviewHistoryListLiveData() {
        return mPreviewHistoryListLiveData;
    }

    public void setPreviewHistoryListLiveData(List<PreviewHistoryAdapter.DataHolder> mPreviewHistoryListLiveData) {
        this.mPreviewHistoryListLiveData.setValue(mPreviewHistoryListLiveData);
    }

    public void loadHistoryAdapterList(Context context) {
        List<HistoryAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<RenameHistory> records = DatabaseHelper.getInstance(context)
                .getRenameHistoryDao()
                .loadAll();

        if (records != null) {
            for (RenameHistory renameHistory : records) {
                HistoryAdapter.DataHolder dataHolder = new HistoryAdapter.DataHolder();
                dataHolder.id = renameHistory.getId();
                dataHolder.taskTitle = renameHistory.getTasktitle();
                dataHolder.outputPath = renameHistory.getOutputPath();
                dataHolder.renameDate = renameHistory.getRenameDate();

                dataHolders.add(dataHolder);
            }
        }

        mHistoryAdapterListLiveData.setValue(dataHolders);
    }

    public void loadSingleHistory(Context context, Long ID) {
        List<PreviewHistoryAdapter.DataHolder> dataHolders = new ArrayList<>();
        RenameHistory records = DatabaseHelper.getInstance(context)
                .getRenameHistoryDao()
                .load(ID);

        if(records.getFileNames() != null || records.getFileNames().size() != 0){
            for(PreviewHistory previewHistory : records.getFileNames()){
                PreviewHistoryAdapter.DataHolder dataHolder = new PreviewHistoryAdapter.DataHolder();
                dataHolder.id = previewHistory.getId();
                dataHolder.taskTitle = previewHistory.getTaskTitle();
                dataHolder.originalFileName = previewHistory.getOriginalFileName();
                dataHolder.newFileName = previewHistory.getNewFileName();
                dataHolder.sourceFileName = previewHistory.getSourceFileName();

                dataHolders.add(dataHolder);
            }

            mPreviewHistoryListLiveData.setValue(dataHolders);
        }

    }
}
