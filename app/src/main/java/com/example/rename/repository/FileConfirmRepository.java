package com.example.rename.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.FileAdapter_Confirm;
import com.example.rename.entitiesModel.entities.file;

import java.util.ArrayList;
import java.util.List;

public class FileConfirmRepository {
    private MutableLiveData<List<FileAdapter_Confirm.DataHolder>> mFileAdapterListLiveData = new MutableLiveData<>();
    private MutableLiveData<List<file>> mFileSelectionConfirmationList = new MutableLiveData<>();
    private List<file> fileList_Confirm;
    private static com.example.rename.repository.FileConfirmRepository mInstance;

    public static com.example.rename.repository.FileConfirmRepository getInstance() {
        if (mInstance == null) {
            synchronized (com.example.rename.repository.FileConfirmRepository.class) {
                mInstance = new com.example.rename.repository.FileConfirmRepository();
            }
        }

        return mInstance;
    }

    public FileConfirmRepository() {
    }

    public MutableLiveData<List<file>> getmFileSelectionConfirmationList() {
        return mFileSelectionConfirmationList;
    }

    public LiveData<List<FileAdapter_Confirm.DataHolder>> getFileAdapterListLiveData() {
        return mFileAdapterListLiveData;
    }

    public List<file> getFileList_Confirm() {
        return fileList_Confirm;
    }

    public void setFileList_Confirm(List<file> fileList_Confirm) {
        this.fileList_Confirm = fileList_Confirm;
    }

    public void loadFileAdapterList(List<file> fileSelectedRecords) {
        List<FileAdapter_Confirm.DataHolder> dataHolders = new ArrayList<>();

        if(fileSelectedRecords != null){
            mFileSelectionConfirmationList.setValue(fileSelectedRecords);
            for(file files : fileSelectedRecords){
                FileAdapter_Confirm.DataHolder dataHolder = new FileAdapter_Confirm.DataHolder();
                dataHolder.id = files.getId();
                dataHolder.filename = files.getFilename() + files.getExtension();
                dataHolder.confirmFlag = files.getConfirmFlag();

                dataHolders.add(dataHolder);
            }
        }

        mFileAdapterListLiveData.setValue(dataHolders);
    }

    public void deleteFile(String Id){
        List<FileAdapter_Confirm.DataHolder> dataHolders = new ArrayList<>();

        //Add new dataholders with dataholder without dataholder of Id specified.
        for(int i=0; i < mFileAdapterListLiveData.getValue().size(); i++){
            if(!mFileAdapterListLiveData.getValue().get(i).id.equals(Id)){
                dataHolders.add(mFileAdapterListLiveData.getValue().get(i));
            }else{
                if(mFileSelectionConfirmationList.getValue().size() != 1){
                    mFileSelectionConfirmationList.getValue().remove(i);
                }
            }
        }
        mFileAdapterListLiveData.setValue(dataHolders);
    }

    public void resetAllVariables(){
        List<FileAdapter_Confirm.DataHolder> dataHolders = new ArrayList<>();
        List<file> files = new ArrayList<>();
        mFileAdapterListLiveData.setValue(dataHolders);
        mFileSelectionConfirmationList.setValue(files);
        fileList_Confirm.removeAll(fileList_Confirm);
    }
}
