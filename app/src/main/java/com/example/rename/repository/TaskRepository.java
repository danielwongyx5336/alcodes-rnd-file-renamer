package com.example.rename.repository;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.MenuItem;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.TaskAdapter;
import com.example.rename.entitiesModel.entities.DaoSession;
import com.example.rename.entitiesModel.entities.InsertChar;
import com.example.rename.entitiesModel.entities.InsertFileSize;
import com.example.rename.entitiesModel.entities.InsertNumber;
import com.example.rename.entitiesModel.entities.NewName;
import com.example.rename.entitiesModel.entities.ReplaceChars;
import com.example.rename.entitiesModel.entities.Task;
import com.example.rename.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {
    private static com.example.rename.repository.TaskRepository mInstance;
    public static final String TAG = com.example.rename.repository.TaskRepository.class.getSimpleName();

    private MutableLiveData<List<TaskAdapter.DataHolder>> mTaskAdapterListLiveData = new MutableLiveData<>();
    private MutableLiveData<List<TaskAdapter.DataHolder>> mTaskAdapterListDialogLiveData = new MutableLiveData<>();
    private MutableLiveData<Task> mTempTaskLiveData = new MutableLiveData<>();
    private Task tempLoadTask;
    private String tempTaskTitle = "";

    public static com.example.rename.repository.TaskRepository getInstance() {
        if (mInstance == null) {
            synchronized (com.example.rename.repository.TaskRepository.class) {
                mInstance = new com.example.rename.repository.TaskRepository();
            }
        }

        return mInstance;
    }

    public TaskRepository() {
    }

    public LiveData<List<TaskAdapter.DataHolder>> getTaskAdapterListLiveData() {
        return mTaskAdapterListLiveData;
    }

    public MutableLiveData<Task> getTempTaskLiveData() {
        return mTempTaskLiveData;
    }

    public void setTempTaskLiveData(Task tempTask) {
        mTempTaskLiveData.setValue(tempTask);
    }

    public String getTempTaskTitle() {
        return tempTaskTitle;
    }

    public void setTempTaskTitle(String tempTaskTitle) {
        this.tempTaskTitle = tempTaskTitle;
    }

    public void setTempLoadTask(Task tempLoadTask) {
        this.tempLoadTask = tempLoadTask;
    }

    public MutableLiveData<List<TaskAdapter.DataHolder>> getTaskAdapterListDialogLiveData() {
        return mTaskAdapterListDialogLiveData;
    }

    public void loadTaskAdapterListDialogLiveData(Context context){
        List<Task> record = DatabaseHelper.getNewInstance(context)
                                            .getTaskDao()
                                            .loadAll();
        List<TaskAdapter.DataHolder> dataHolders = new ArrayList<>();
        if(record != null){
            int i = 0;
            for(Task tasks : record){
                if(tasks.getEnableFlag()){
                    TaskAdapter.DataHolder dataholder = new TaskAdapter.DataHolder();
                    dataholder.id = Long.valueOf(i);
                    dataholder.taskID = tasks.getId();
                    dataholder.title = tasks.getTitle();
                    dataHolders.add(dataholder);
                    i++;
                }
            }
        }

        mTaskAdapterListDialogLiveData.setValue(dataHolders);
    }

    public void addTempCriteriaToLiveData(Context context, ReplaceChars replaceChars, NewName newName,
                                          InsertNumber insertNumber, InsertFileSize insertFileSize,
                                          InsertChar insertChar){
        List<TaskAdapter.DataHolder> tempLiveData;
        int incrementID = 0;
        if(mTaskAdapterListLiveData.getValue() != null && mTaskAdapterListLiveData.getValue().size() != 0){
            tempLiveData = mTaskAdapterListLiveData.getValue();
            incrementID = mTaskAdapterListLiveData.getValue().size() - 1;
        }else{
            tempLiveData = new ArrayList<>();
        }

        if(replaceChars != null){
            TaskAdapter.DataHolder dataHolder = new TaskAdapter.DataHolder();
            dataHolder.id = Long.valueOf(incrementID);
            dataHolder.title = replaceChars.getTitle();
            dataHolder.criteriaTableID = replaceChars.getId();
            dataHolder.criteriaTableName = "replaceChars";
            incrementID += 1;
            tempLiveData.add(dataHolder);
        }
        if(newName != null){
            TaskAdapter.DataHolder dataHolder = new TaskAdapter.DataHolder();
            dataHolder.id = Long.valueOf(incrementID);
            dataHolder.title = newName.getTitle();
            dataHolder.criteriaTableID = newName.getId();
            dataHolder.criteriaTableName = "newName";
            incrementID += 1;
            tempLiveData.add(dataHolder);
        }
        if(insertNumber != null){
            TaskAdapter.DataHolder dataHolder = new TaskAdapter.DataHolder();
            dataHolder.id = Long.valueOf(incrementID);
            dataHolder.title = insertNumber.getTitle();
            dataHolder.criteriaTableID = insertNumber.getId();
            dataHolder.criteriaTableName = "insertNumber";
            incrementID += 1;
            tempLiveData.add(dataHolder);
        }
        if(insertFileSize != null){
            TaskAdapter.DataHolder dataHolder = new TaskAdapter.DataHolder();
            dataHolder.id = Long.valueOf(incrementID);
            dataHolder.title = insertFileSize.getTitle();
            dataHolder.criteriaTableID = insertFileSize.getId();
            dataHolder.criteriaTableName = "insertFileSize";
            incrementID += 1;
            tempLiveData.add(dataHolder);
        }
        if(insertChar != null){
            TaskAdapter.DataHolder dataHolder = new TaskAdapter.DataHolder();
            dataHolder.id = Long.valueOf(incrementID);
            dataHolder.title = insertChar.getTitle();
            dataHolder.criteriaTableID = insertChar.getId();
            dataHolder.criteriaTableName = "insertChar";
            incrementID += 1;
            tempLiveData.add(dataHolder);
        }

        mTaskAdapterListLiveData.setValue(tempLiveData);
    }

    public void newTask(Context context, String title, Task newTask){
        //Merge Import Task with current new task criteria creation **
        newTask.setId(null);
        newTask.setTitle(title);
        if(tempLoadTask != null){
            if(tempLoadTask.getInsertChar().size() != 0){
                int x =0;
                do{
                    for(int i=0; i < newTask.getInsertCharNoDb().size(); i++){
                        if(newTask.getInsertCharNoDb().get(i).getId() != tempLoadTask.getInsertChar().get(x).getId()){
                            newTask.getInsertCharNoDb().add(tempLoadTask.getInsertChar().get(x));
                        }
                    }
                    x++;
                }while(x > tempLoadTask.getInsertChar().size());
            }
            if(tempLoadTask.getInsertFileSize().size() != 0){
                int x=0;
                do{
                    for(int i=0; i < newTask.getInsertFileSizeNoDb().size(); i++){
                        if(newTask.getInsertFileSizeNoDb().get(i).getId() != tempLoadTask.getInsertFileSize().get(x).getId()){
                            newTask.getInsertFileSizeNoDb().add(tempLoadTask.getInsertFileSize().get(x));
                        }
                    }
                    x++;
                }while(x > tempLoadTask.getInsertFileSize().size());
            }
            if(tempLoadTask.getInsertNumbers().size() != 0){
                int x=0;
                do{
                    for(int i=0; i < newTask.getInsertNumberNoDb().size(); i++){
                        if(newTask.getInsertNumberNoDb().get(i).getId() != tempLoadTask.getInsertNumbers().get(x).getId()){
                            newTask.getInsertNumberNoDb().add(tempLoadTask.getInsertNumbers().get(x));
                        }
                    }
                    x++;
                }while(x > tempLoadTask.getInsertNumbers().size());
            }
            if(tempLoadTask.getNewName().size() != 0){
                int x=0;
                do{
                    for(int i=0; i < newTask.getNewNameNoDb().size(); i++){
                        if(newTask.getNewNameNoDb().get(i).getId() != tempLoadTask.getNewName().get(x).getId()){
                            newTask.getNewNameNoDb().add(tempLoadTask.getNewName().get(x));
                        }
                    }
                    x++;
                }while(x > tempLoadTask.getNewName().size());
            }
            if(tempLoadTask.getReplaceChars().size() != 0){
                int x=0;
                do{
                    for(int i=0; i < newTask.getReplaceCharsNoDB().size(); i++){
                        if(newTask.getReplaceCharsNoDB().get(i).getId() != tempLoadTask.getReplaceChars().get(x).getId()){
                            newTask.getReplaceCharsNoDB().add(tempLoadTask.getReplaceChars().get(x));
                        }
                    }
                    x++;
                }while(x > tempLoadTask.getReplaceChars().size());
            }
        }
        //Merge Import Task with current new task criteria creation **

        //Get Insert New Task and Get ID **
        DatabaseHelper.getInstance(context)
                .getTaskDao()
                .insert(newTask);

        Long taskID = newTask.getId();

        //Save tempTask for preview fragment**
        mTempTaskLiveData.setValue(newTask);
        //Save tempTask for preview fragment**

        //Task Criteria Insertion **
        if(newTask.getInsertCharNoDb().size() != 0){
            for(int i=0; i < newTask.getInsertCharNoDb().size(); i++){
                InsertChar insertChar = new InsertChar();
                insertChar.setTitle(newTask.getInsertCharNoDb().get(i).getTitle());
                insertChar.setInsertCharacter(newTask.getInsertCharNoDb().get(i).getInsertCharacter());
                insertChar.setInsertIndex(newTask.getInsertCharNoDb().get(i).getInsertIndex());
                insertChar.setTaskID(taskID);
                DatabaseHelper.getInstance(context)
                        .getInsertCharDao()
                        .insert(insertChar);
            }
        }
        if(newTask.getInsertFileSizeNoDb().size() != 0){
            for(int i=0; i < newTask.getInsertFileSizeNoDb().size(); i++) {
                InsertFileSize insertFileSize = new InsertFileSize();
                insertFileSize.setTitle(newTask.getInsertFileSizeNoDb().get(i).getTitle());
                insertFileSize.setUnit(newTask.getInsertFileSizeNoDb().get(i).getUnit());
                insertFileSize.setInsertIndex(newTask.getInsertFileSizeNoDb().get(i).getInsertIndex());
                insertFileSize.setTaskID(taskID);
                DatabaseHelper.getInstance(context)
                        .getInsertFileSizeDao()
                        .insert(insertFileSize);
            }
        }
        if(newTask.getInsertNumberNoDb().size() != 0){
            for(int i=0; i < newTask.getInsertNumberNoDb().size(); i++){
                InsertNumber insertNumber = new InsertNumber();
                insertNumber.setTitle(newTask.getInsertNumberNoDb().get(i).getTitle());
                insertNumber.setInsertNumbers(newTask.getInsertNumberNoDb().get(i).getInsertNumbers());
                insertNumber.setInsertIndex(newTask.getInsertNumberNoDb().get(i).getInsertIndex());
                insertNumber.setTaskID(taskID);
                DatabaseHelper.getInstance(context)
                        .getInsertNumberDao()
                        .insert(insertNumber);
            }
        }
        if(newTask.getNewNameNoDb().size() != 0){
            for(int i=0; i < newTask.getNewNameNoDb().size(); i++){
                NewName newName = new NewName();
                newName.setTitle(newTask.getNewNameNoDb().get(i).getTitle());
                newName.setNewName(newTask.getNewNameNoDb().get(i).getNewName());
                newName.setTaskID(taskID);
                DatabaseHelper.getInstance(context)
                        .getNewNameDao()
                        .insert(newName);
            }
        }
        if(newTask.getReplaceCharsNoDB().size() != 0){
            for(int i=0; i < newTask.getReplaceCharsNoDB().size();i++){
                ReplaceChars replaceChars = new ReplaceChars();
                replaceChars.setTitle(newTask.getReplaceCharsNoDB().get(i).getTitle());
                replaceChars.setOldCharacter(newTask.getReplaceCharsNoDB().get(i).getOldCharacter());
                replaceChars.setReplaceCharacter(newTask.getReplaceCharsNoDB().get(i).getReplaceCharacter());
                replaceChars.setTaskID(taskID);

                DatabaseHelper.getInstance(context)
                        .getReplaceCharsDao()
                        .insert(replaceChars);
            }
        }
        //Task Criteria Insertion **
    }

    public void deleteCriteria(Context context, Long id) {
        Task tempTask = mTempTaskLiveData.getValue();
        for(int i=0; i < mTaskAdapterListLiveData.getValue().size(); i++){
            if(mTaskAdapterListLiveData.getValue().get(i).id == id){
                if(tempTask != null){
                    if(mTaskAdapterListLiveData.getValue().get(i).criteriaTableName.equals("replaceChars")){
                        for(int x=0; x < tempTask.getReplaceChars().size();x++){
                            if(tempTask.getReplaceChars().get(x).getId() == mTaskAdapterListLiveData.getValue().get(i).criteriaTableID){
                                tempTask.getReplaceChars().remove(x);
                                break;
                            }
                        }
                    }
                    if(mTaskAdapterListLiveData.getValue().get(i).criteriaTableName.equals("insertFileSize")){
                        for(int x=0; x < tempTask.getInsertFileSize().size();x++){
                            if(tempTask.getInsertFileSize().get(x).getId() == mTaskAdapterListLiveData.getValue().get(i).criteriaTableID){
                                tempTask.getInsertFileSize().remove(x);
                                break;
                            }
                        }
                    }
                    if(mTaskAdapterListLiveData.getValue().get(i).criteriaTableName.equals("insertNumber")){
                        for(int x=0; x < tempTask.getInsertNumbers().size();x++){
                            if(tempTask.getInsertNumbers().get(x).getId() == mTaskAdapterListLiveData.getValue().get(i).criteriaTableID){
                                tempTask.getInsertNumbers().remove(x);
                                break;
                            }
                        }
                    }
                    if(mTaskAdapterListLiveData.getValue().get(i).criteriaTableName.equals("insertChar")){
                        for(int x=0; x < tempTask.getInsertChar().size();x++){
                            if(tempTask.getInsertChar().get(x).getId() == mTaskAdapterListLiveData.getValue().get(i).criteriaTableID){
                                tempTask.getInsertChar().remove(x);
                                break;
                            }
                        }
                    }
                    if(mTaskAdapterListLiveData.getValue().get(i).criteriaTableName.equals("newName")){
                        for(int x=0; x < tempTask.getNewName().size();x++){
                            if(tempTask.getNewName().get(x).getId() == mTaskAdapterListLiveData.getValue().get(i).criteriaTableID){
                                tempTask.getNewName().remove(x);
                                break;
                            }
                        }
                    }
                }

                mTaskAdapterListLiveData.getValue().remove(i);
                break;
            }
        }
        mTempTaskLiveData.setValue(tempTask);
        mTaskAdapterListLiveData.setValue(mTaskAdapterListLiveData.getValue());
    }

    public Task loadTaskAdapterByIdList(Context context, Long taskID) {
        List<TaskAdapter.DataHolder> dataHolders = new ArrayList<>();
        Task tasks = DatabaseHelper.getNewInstance(context)
                .getTaskDao()
                .load(taskID);

        //Store temporary current imported task data **
        tempLoadTask = tasks;
        mTempTaskLiveData.setValue(tasks);
        //Store temporary current imported task data **

        //Load into RecycleView List **
        //Reset DataHolders**
        mTaskAdapterListLiveData.setValue(dataHolders);
        //Reset DataHolders**
        int x = 0;
        if (tasks.getReplaceChars() != null) {
            for(int i =0; i < tasks.getReplaceChars().size(); i++){
                addTempCriteriaToLiveData(context, tasks.getReplaceChars().get(i),
                        null, null, null, null);
            }
        }
        if (tasks.getNewName() != null) {
            for(int i=0; i < tasks.getNewName().size(); i++){
                addTempCriteriaToLiveData(context, null,
                        tasks.getNewName().get(i), null, null, null);
            }
        }
        if (tasks.getInsertNumbers() != null) {
            for(int i=0;i < tasks.getInsertNumbers().size(); i++){
                addTempCriteriaToLiveData(context, null,
                        null, tasks.getInsertNumbers().get(i), null, null);
            }
        }
        if (tasks.getInsertFileSize() != null) {
            for(int i=0; i < tasks.getInsertFileSize().size(); i++){
                addTempCriteriaToLiveData(context, null,
                        null, null, tasks.getInsertFileSize().get(i), null);
            }
        }
        if(tasks.getInsertChar() != null){
            for(int i=0; i < tasks.getInsertChar().size(); i++){
                addTempCriteriaToLiveData(context, null,
                        null, null, null, tasks.getInsertChar().get(i));
            }
        }
        //Load into RecycleView List **

        return tempLoadTask;
    }

    public void deleteTask(Context context, Long id){
        Task tasks = DatabaseHelper.getInstance(context)
                .getTaskDao()
                .load(id);

        tasks.setEnableFlag(false);
        DatabaseHelper.getInstance(context)
                .getTaskDao()
                .update(tasks);

        loadTaskAdapterListDialogLiveData(context);
    }

    public void resetAllVariables(){
        List<TaskAdapter.DataHolder> dataHolders = new ArrayList<>();

        mTaskAdapterListLiveData.setValue(dataHolders);
        mTempTaskLiveData.setValue(new Task(""));
        setTempTaskTitle("");
        setTempLoadTask(new Task(""));
    }

}
