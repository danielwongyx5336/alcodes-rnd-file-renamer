package com.example.rename.repository;

import android.content.ClipData;
import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.FileAdapter_Confirm;
import com.example.rename.adapters.PreviewAdapter;
import com.example.rename.adapters.TaskAdapter;
import com.example.rename.entitiesModel.entities.InsertChar;
import com.example.rename.entitiesModel.entities.InsertFileSize;
import com.example.rename.entitiesModel.entities.InsertNumber;
import com.example.rename.entitiesModel.entities.NewName;
import com.example.rename.entitiesModel.entities.PreviewHistory;
import com.example.rename.entitiesModel.entities.RenameHistory;
import com.example.rename.entitiesModel.entities.ReplaceChars;
import com.example.rename.entitiesModel.entities.file;
import com.example.rename.entitiesModel.entities.Task;
import com.example.rename.entitiesModel.entities.TaskDao;
import com.example.rename.fragments.PreviewFragment;
import com.example.rename.utils.DatabaseHelper;
import com.example.rename.utils.FileNameHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import timber.log.Timber;

public class PreviewRepository {
    private MutableLiveData<List<PreviewAdapter.DataHolder>> mPreviewAdapterLiveData = new MutableLiveData<>();
    private MutableLiveData<String> mOutputPathLiveData = new MutableLiveData<>();
    private static PreviewRepository mInstance;
    private FileNameHelper fileNameHelper;

    private List<file> tempFileSelectedList;
    private Task tempRecords;

    public static PreviewRepository getInstance() {
        if (mInstance == null) {
            synchronized (PreviewRepository.class) {
                mInstance = new PreviewRepository();
            }
        }

        return mInstance;
    }

    public PreviewRepository() {
    }

    public MutableLiveData<String> getOutputPath() {
        return mOutputPathLiveData;
    }

    public void setOutputPath(String outputPath) {
        mOutputPathLiveData.setValue(outputPath);
    }

    public MutableLiveData<List<PreviewAdapter.DataHolder>> getmPreviewAdapterLiveData() {
        return mPreviewAdapterLiveData;
    }

    public void loadPreviewAdapterList(Context context, List<file> fileSelectedRecords, Task tempTask) {
        List<PreviewAdapter.DataHolder> dataHolders = new ArrayList<>();
        fileNameHelper = new FileNameHelper();

        Task records = tempTask;
        tempRecords = tempTask;

        if(fileSelectedRecords != null){
            tempFileSelectedList = fileSelectedRecords;
            int x=0;
            for(file files : fileSelectedRecords){
                if(files.getConfirmFlag()){
                    String previewFileNameChanges = files.getFilename();
                    if(records.getInsertChar() != null){
                        for(InsertChar insertChar : records.getInsertChar()){
                            //Insert Begin **
                            if(insertChar.getInsertIndex() == 0){
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertChar.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();
                                dataHolder.newFileName = insertChar.getInsertCharacter() + previewFileNameChanges + files.getExtension();
                                dataHolders.add(dataHolder);
                                previewFileNameChanges = insertChar.getInsertCharacter() + previewFileNameChanges;
                                x++;
                            }//Insert Begin **
                            //Insert Back **
                            else if(insertChar.getInsertIndex() == -1){
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertChar.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();
                                dataHolder.newFileName = previewFileNameChanges + insertChar.getInsertCharacter() + files.getExtension();
                                dataHolders.add(dataHolder);
                                previewFileNameChanges += insertChar.getInsertCharacter();
                                x++;
                            }//Insert Back **
                            //Insert Custom **
                            else{
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertChar.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();

                                if(insertChar.getInsertIndex() > previewFileNameChanges.length()){
                                    String tempNewFileName = previewFileNameChanges;
                                    for(int z= previewFileNameChanges.length() -1; z<insertChar.getInsertIndex(); z++){
                                        tempNewFileName += "_";
                                    }
                                    dataHolder.newFileName = tempNewFileName + insertChar.getInsertCharacter() + files.getExtension();
                                    previewFileNameChanges = tempNewFileName + insertChar.getInsertCharacter();
                                }else{
                                    dataHolder.newFileName = previewFileNameChanges.substring(0, insertChar.getInsertIndex())
                                            + insertChar.getInsertCharacter()
                                            + previewFileNameChanges.substring(insertChar.getInsertIndex(), previewFileNameChanges.length()-1)
                                            + files.getExtension();
                                    previewFileNameChanges = previewFileNameChanges.substring(0, insertChar.getInsertIndex())
                                            + insertChar.getInsertCharacter()
                                            + previewFileNameChanges.substring(insertChar.getInsertIndex(), previewFileNameChanges.length()-1)
                                            + files.getExtension();
                                }
                                dataHolders.add(dataHolder);
                            }
                            //Insert Custom **
                        }
                    }
                    if(records.getInsertFileSize() != null){
                        for(InsertFileSize insertFileSize : records.getInsertFileSize()){
                            String fileSizeFormat = "";
                            if(insertFileSize.getUnit().equals(".MB")){
                                fileSizeFormat = fileNameHelper.fileSizeBytesConverter(files.getFileSize(), "MB");
                            }else if(insertFileSize.getUnit().equals(".KB")){
                                fileSizeFormat = fileNameHelper.fileSizeBytesConverter(files.getFileSize(), "KB");
                            }else{
                                fileSizeFormat = fileNameHelper.fileSizeBytesConverter(files.getFileSize(), "Bytes");
                            }

                            //Insert Begin **
                            if(insertFileSize.getInsertIndex() == 0){
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertFileSize.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();
                                dataHolder.newFileName = fileSizeFormat + previewFileNameChanges + files.getExtension();
                                dataHolders.add(dataHolder);
                                previewFileNameChanges = fileSizeFormat + previewFileNameChanges;
                                x++;
                            }//Insert Begin **
                            //Insert Back **
                            else if(insertFileSize.getInsertIndex() == -1){
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertFileSize.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();
                                dataHolder.newFileName = previewFileNameChanges + fileSizeFormat + files.getExtension();
                                dataHolders.add(dataHolder);
                                previewFileNameChanges += fileSizeFormat;
                                x++;
                            }//Insert Back **
                            //Insert Custom **
                            else{
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertFileSize.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();

                                if(insertFileSize.getInsertIndex() > previewFileNameChanges.length()){
                                    String tempNewFileName = previewFileNameChanges;
                                    for(int z= previewFileNameChanges.length() -1; z < insertFileSize.getInsertIndex(); z++){
                                        tempNewFileName += "_";
                                    }
                                    dataHolder.newFileName = tempNewFileName + fileSizeFormat + files.getExtension();
                                    previewFileNameChanges = tempNewFileName + fileSizeFormat;
                                }else{
                                    dataHolder.newFileName = previewFileNameChanges.substring(0, insertFileSize.getInsertIndex())
                                            + fileSizeFormat
                                            + previewFileNameChanges.substring(insertFileSize.getInsertIndex(), previewFileNameChanges.length()-1)
                                            + files.getExtension();
                                    previewFileNameChanges = previewFileNameChanges.substring(0, insertFileSize.getInsertIndex())
                                            + fileSizeFormat
                                            + previewFileNameChanges.substring(insertFileSize.getInsertIndex(), previewFileNameChanges.length()-1)
                                            + files.getExtension();
                                }
                                dataHolders.add(dataHolder);
                            }
                            //Insert Custom **
                        }
                    }
                    if(records.getInsertNumbers() != null){
                        for(InsertNumber insertNumber : records.getInsertNumbers()){
                            //Insert Begin **
                            if(insertNumber.getInsertIndex() == 0){
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertNumber.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();
                                dataHolder.newFileName = insertNumber.getInsertNumbers() + previewFileNameChanges + files.getExtension();
                                dataHolders.add(dataHolder);
                                previewFileNameChanges = insertNumber.getInsertNumbers() + previewFileNameChanges;
                                x++;
                            }//Insert Begin **
                            //Insert Back **
                            else if(insertNumber.getInsertIndex() == -1){
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertNumber.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();
                                dataHolder.newFileName = previewFileNameChanges + insertNumber.getInsertNumbers() + files.getExtension();
                                dataHolders.add(dataHolder);
                                previewFileNameChanges += insertNumber.getInsertNumbers();
                                x++;
                            }//Insert Back **
                            //Insert Custom **
                            else{
                                PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                                dataHolder.id = Long.valueOf(x);
                                dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                                dataHolder.taskTitle = insertNumber.getTitle();
                                dataHolder.originalFileName = previewFileNameChanges + files.getExtension();

                                if(insertNumber.getInsertIndex() > previewFileNameChanges.length()){
                                    String tempNewFileName = previewFileNameChanges;
                                    for(int z= previewFileNameChanges.length() -1; z<insertNumber.getInsertIndex(); z++){
                                        tempNewFileName += "_";
                                    }
                                    dataHolder.newFileName = tempNewFileName + insertNumber.getInsertNumbers() + files.getExtension();
                                    previewFileNameChanges = tempNewFileName + insertNumber.getInsertNumbers();
                                }else{
                                    dataHolder.newFileName = previewFileNameChanges.substring(0, insertNumber.getInsertIndex())
                                            + insertNumber.getInsertNumbers()
                                            + previewFileNameChanges.substring(insertNumber.getInsertIndex(), previewFileNameChanges.length()-1)
                                            + files.getExtension();
                                    previewFileNameChanges = previewFileNameChanges.substring(0, insertNumber.getInsertIndex())
                                            + insertNumber.getInsertNumbers()
                                            + previewFileNameChanges.substring(insertNumber.getInsertIndex(), previewFileNameChanges.length()-1)
                                            + files.getExtension();
                                }
                                dataHolders.add(dataHolder);
                            }
                            //Insert Custom **
                        }
                    }
                    if(records.getNewName() != null){
                        for(NewName newName : records.getNewName()){
                            PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                            dataHolder.id = Long.valueOf(x);
                            dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                            dataHolder.taskTitle = newName.getTitle();
                            dataHolder.originalFileName = previewFileNameChanges + files.getExtension();
                            dataHolder.newFileName = newName.getNewName() + files.getExtension();
                            dataHolders.add(dataHolder);
                            previewFileNameChanges = newName.getNewName();
                            x++;
                        }
                    }
                    if(records.getReplaceChars() != null){
                        for(ReplaceChars replaceChars : records.getReplaceChars()){
                            PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
                            dataHolder.id = Long.valueOf(x);
                            dataHolder.sourceFileName = files.getFilename() + files.getExtension();
                            dataHolder.taskTitle = replaceChars.getTitle();
                            dataHolder.originalFileName = previewFileNameChanges + files.getExtension();

                            if(previewFileNameChanges.contains(replaceChars.getOldCharacter())){
                                previewFileNameChanges = previewFileNameChanges.replace(replaceChars.getOldCharacter(), replaceChars.getReplaceCharacter());
                            }
                            dataHolder.newFileName = previewFileNameChanges + files.getExtension();

                            dataHolders.add(dataHolder);
                            x++;
                        }
                    }
                }
            }

        }

        mPreviewAdapterLiveData.setValue(dataHolders);
    }

    public Boolean renameProcess(Context context){
        int i=0;
        Boolean failedFlag = false;
        for(file files : tempFileSelectedList){
            if(files.getConfirmFlag()){
                String fileNameChanges = files.getFilename();
                if(tempRecords.getInsertChar() != null){
                    for(InsertChar insertChar : tempRecords.getInsertChar()){
                        //Insert Begin **
                        if(insertChar.getInsertIndex() == 0){
                            fileNameChanges = insertChar.getInsertCharacter() + fileNameChanges;
                        }//Insert Begin **
                        //Insert Back **
                        else if(insertChar.getInsertIndex() == -1){
                            fileNameChanges += insertChar.getInsertCharacter();
                        }//Insert Back **
                        //Insert Custom **
                        else{
                            if(insertChar.getInsertIndex() > fileNameChanges.length()){
                                String tempNewFileName = fileNameChanges;
                                for(int z= fileNameChanges.length() -1; z<insertChar.getInsertIndex(); z++){
                                    tempNewFileName += "_";
                                }
                                fileNameChanges = tempNewFileName + insertChar.getInsertCharacter();
                            }else{
                                fileNameChanges = fileNameChanges.substring(0, insertChar.getInsertIndex())
                                        + insertChar.getInsertCharacter()
                                        + fileNameChanges.substring(insertChar.getInsertIndex(), fileNameChanges.length()-1);
                            }
                        }
                        //Insert Custom **
                    }
                }
                if(tempRecords.getInsertFileSize() != null){
                    for(InsertFileSize insertFileSize : tempRecords.getInsertFileSize()){
                        String fileSizeFormat = "";
                        if(insertFileSize.getUnit().equals(".MB")){
                            fileSizeFormat = fileNameHelper.fileSizeBytesConverter(files.getFileSize(), "MB");
                        }else if(insertFileSize.getUnit().equals(".KB")){
                            fileSizeFormat = fileNameHelper.fileSizeBytesConverter(files.getFileSize(), "KB");
                        }else{
                            fileSizeFormat = fileNameHelper.fileSizeBytesConverter(files.getFileSize(), "Bytes");
                        }

                        //Insert Begin **
                        if(insertFileSize.getInsertIndex() == 0){
                            fileNameChanges = fileSizeFormat + fileNameChanges;
                        }//Insert Begin **
                        //Insert Back **
                        else if(insertFileSize.getInsertIndex() == -1){
                            fileNameChanges += fileSizeFormat;
                        }//Insert Back **
                        //Insert Custom **
                        else{
                            if(insertFileSize.getInsertIndex() > fileNameChanges.length()){
                                String tempNewFileName = fileNameChanges;
                                for(int z= fileNameChanges.length() -1; z < insertFileSize.getInsertIndex(); z++){
                                    tempNewFileName += "_";
                                }
                                fileNameChanges = tempNewFileName + fileSizeFormat;
                            }else{
                                fileNameChanges = fileNameChanges.substring(0, insertFileSize.getInsertIndex())
                                        + fileSizeFormat
                                        + fileNameChanges.substring(insertFileSize.getInsertIndex(), fileNameChanges.length()-1);
                            }
                        }
                        //Insert Custom **
                    }
                }
                if(tempRecords.getInsertNumbers() != null){
                    for(InsertNumber insertNumber : tempRecords.getInsertNumbers()){
                        //Insert Begin **
                        if(insertNumber.getInsertIndex() == 0){
                            fileNameChanges = insertNumber.getInsertNumbers() + fileNameChanges;
                        }//Insert Begin **
                        //Insert Back **
                        else if(insertNumber.getInsertIndex() == -1){
                            fileNameChanges += insertNumber.getInsertNumbers();
                        }//Insert Back **
                        //Insert Custom **
                        else{
                            if(insertNumber.getInsertIndex() > fileNameChanges.length()){
                                String tempNewFileName = fileNameChanges;
                                for(int z= fileNameChanges.length() -1; z<insertNumber.getInsertIndex(); z++){
                                    tempNewFileName += "_";
                                }
                                fileNameChanges = tempNewFileName + insertNumber.getInsertNumbers();
                            }else{
                                fileNameChanges = fileNameChanges.substring(0, insertNumber.getInsertIndex())
                                        + insertNumber.getInsertNumbers()
                                        + fileNameChanges.substring(insertNumber.getInsertIndex(), fileNameChanges.length()-1);
                            }
                        }
                        //Insert Custom **
                    }
                }
                if(tempRecords.getNewName() != null){
                    for(NewName newName : tempRecords.getNewName()){
                        fileNameChanges = newName.getNewName();
                    }
                }
                if(tempRecords.getReplaceChars() != null) {
                    for (ReplaceChars replaceChars : tempRecords.getReplaceChars()) {
                        if (fileNameChanges.contains(replaceChars.getOldCharacter())) {
                            fileNameChanges = fileNameChanges.replace(replaceChars.getOldCharacter(), replaceChars.getReplaceCharacter());
                        }
                    }
                }

                //Final Appended Filename Changes for rename **
                try{
                    Uri newUri = fileNameHelper.fileRenaming(context, files.getUri(), fileNameChanges,
                            files.getExtension());
                    if(newUri == null){
                        throw new Exception("Failed to rename.");
                    }
                    tempFileSelectedList.get(i).setUri(newUri);
                }catch(Exception e){
                    failedFlag = true;
                    break;
                }
                //Final Appended Filename Changes for rename **
                i++;
            }
        }
        if(failedFlag){
            return false;
        }else{
            return true;
        }
    }

    public Boolean fileRenamedOutputLocation(Context context, Uri uri){
        try{
            tempFileSelectedList = fileNameHelper.fileMove(context, uri, tempFileSelectedList);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    public void savedPreviewToHistory(Context context){
        RenameHistory renameHistory = new RenameHistory();
        renameHistory.setOutputPath(mOutputPathLiveData.getValue());
        renameHistory.setTasktitle(tempRecords.getTitle());
        renameHistory.setTask(tempRecords);
        renameHistory.setTaskID(tempRecords.getId());
        renameHistory.setRenameDate(Calendar.getInstance().getTime());

        DatabaseHelper.getInstance(context)
                .getRenameHistoryDao()
                .insert(renameHistory);

        Long renameHistoryID = renameHistory.getId();

        for(PreviewAdapter.DataHolder dataHolder : mPreviewAdapterLiveData.getValue()){
            PreviewHistory previewHistory = new PreviewHistory();
            previewHistory.setTaskTitle(dataHolder.taskTitle);
            previewHistory.setNewFileName(dataHolder.newFileName);
            previewHistory.setOriginalFileName(dataHolder.originalFileName);
            previewHistory.setSourceFileName(dataHolder.sourceFileName);
            previewHistory.setRenameHistoryID(renameHistoryID);

            DatabaseHelper.getInstance(context)
                    .getPreviewHistoryDao()
                    .insert(previewHistory);
        }
    }
}
