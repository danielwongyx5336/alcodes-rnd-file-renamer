package com.example.rename.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.rename.adapters.FileAdapter_Select;
import com.example.rename.entitiesModel.entities.file;

import java.util.ArrayList;
import java.util.List;

public class FileSelectRepository {
    private MutableLiveData<List<FileAdapter_Select.DataHolder>> mFileAdapterListLiveData = new MutableLiveData<>();
    private List<file> fileList_Select;
    private static FileSelectRepository mInstance;

    public static FileSelectRepository getInstance(){
        if(mInstance == null){
            synchronized (FileSelectRepository.class){
                mInstance = new FileSelectRepository();
            }
        }

        return mInstance;
    }

    public LiveData<List<FileAdapter_Select.DataHolder>> getFileAdapterListLiveData() {
        return mFileAdapterListLiveData;
    }

    public List<file> getFileList_Select() {
        return fileList_Select;
    }

    public void setFileList_Select(List<file> fileList_Select) {
        this.fileList_Select = fileList_Select;
    }

    public void loadFileAdapterList(List<file> fileSelectedRecords) {
        List<FileAdapter_Select.DataHolder> dataHolders = new ArrayList<>();

        //Check first time
        if(fileSelectedRecords != null){
            for(file files : fileSelectedRecords){
                FileAdapter_Select.DataHolder dataHolder = new FileAdapter_Select.DataHolder();
                dataHolder.id = files.getId();
                dataHolder.filename = files.getFilename() + files.getExtension();
                dataHolder.confirmFlag = files.getConfirmFlag();
                dataHolders.add(dataHolder);
            }
            mFileAdapterListLiveData.setValue(dataHolders);
        }
    }

    public void deleteFile(String Id){
        List<FileAdapter_Select.DataHolder> dataHolders = new ArrayList<>();

        //Add new dataholders with dataholder without dataholder of Id specified.
        for(int i=0; i < mFileAdapterListLiveData.getValue().size(); i++){
            if(!mFileAdapterListLiveData.getValue().get(i).id.equals(Id)){
                dataHolders.add(mFileAdapterListLiveData.getValue().get(i));
            }
        }
        mFileAdapterListLiveData.setValue(dataHolders);
    }

    public void resetAllVariables(){
        List<FileAdapter_Select.DataHolder> dataHolders = new ArrayList<>();

        mFileAdapterListLiveData.setValue(dataHolders);
        fileList_Select.removeAll(fileList_Select);
    }
}
