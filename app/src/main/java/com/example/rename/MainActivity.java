package com.example.rename;

import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.rename.adapters.FileAdapter_Confirm;
import com.example.rename.adapters.FileAdapter_Select;
import com.example.rename.adapters.PreviewAdapter;
import com.example.rename.adapters.TaskAdapter;
import com.example.rename.entitiesModel.entities.Task;
import com.example.rename.fragments.FileSelectionFragment;
import com.example.rename.fragments.HistoryFragment;
import com.example.rename.fragments.PreviewFragment;
import com.example.rename.fragments.RenameTaskFragment;
import com.example.rename.viewModels.FileViewConfirmModel.FileViewConfirmModel;
import com.example.rename.viewModels.FileViewConfirmModel.FileViewConfirmModelFactory;
import com.example.rename.viewModels.FileViewSelectModel.FileViewSelectModel;
import com.example.rename.viewModels.FileViewSelectModel.FileViewSelectModelFactory;
import com.example.rename.viewModels.PreviewViewModel.PreviewViewModel;
import com.example.rename.viewModels.PreviewViewModel.PreviewViewModelFactory;
import com.example.rename.viewModels.TaskViewModel.TaskViewModel;
import com.example.rename.viewModels.TaskViewModel.TaskViewModelFactory;
import com.google.android.material.navigation.NavigationView;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FileSelectionFragment.Callbacks, RenameTaskFragment.Callbacks, PreviewFragment.Callbacks {

    private AppBarConfiguration mAppBarConfiguration;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawer;

    @BindView(R.id.nav_view)
    protected NavigationView navigationView;

    @BindView(R.id.host_fragment)
    protected FrameLayout host_fragment;

    private FragmentManager fragmentManager = getSupportFragmentManager();

    private FileSelectionFragment fileSelectionFragment;

    private FileViewConfirmModel mViewModel_FileConfirmList;
    private FileViewSelectModel mViewModel_FileSelectList;
    private TaskViewModel mViewModel_TaskList;
    private PreviewViewModel mViewModel_PreviewList;

    private Boolean emptyPreviewFragmentFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initView();
        initViewModel();
    }

    private void initView() {
        setSupportActionBar(toolbar);
        setTitle("Rename Builder");

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawer,
                toolbar, 0, 0);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initViewModel() {
        mViewModel_FileConfirmList = new ViewModelProvider(this, new FileViewConfirmModelFactory(getApplication())).get(FileViewConfirmModel.class);

        mViewModel_TaskList = new ViewModelProvider(this, new TaskViewModelFactory(getApplication())).get(TaskViewModel.class);

        mViewModel_FileSelectList = new ViewModelProvider(this, new FileViewSelectModelFactory(getApplication())).get(FileViewSelectModel.class);

        mViewModel_PreviewList = new ViewModelProvider(this, new PreviewViewModelFactory(getApplication())).get(PreviewViewModel.class);

        if(mViewModel_FileConfirmList.getmFileSelectionConfirmationList().getValue() == null
                || mViewModel_FileConfirmList.getmFileSelectionConfirmationList().getValue().size() == 0
                || mViewModel_TaskList.getTempTaskLiveData().getValue() == null){
            emptyPreviewFragmentFlag = true;
            navigationView.getMenu().getItem(3).setEnabled(false);
        }else{
            emptyPreviewFragmentFlag = false;
            navigationView.getMenu().getItem(3).setEnabled(true);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        onNavigationItemSelected(navigationView.getMenu().getItem(1).setChecked(true));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        super.onAttachFragment(fragment);
        if(fragment instanceof FileSelectionFragment){
            ((FileSelectionFragment) fragment).setCallbacks(this);
        }else if(fragment instanceof RenameTaskFragment){
            ((RenameTaskFragment) fragment).setCallbacks(this);
        }else if(fragment instanceof PreviewFragment){
            ((PreviewFragment) fragment).setCallbacks(this);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.nav_builder){
            toolbar.setTitle("Rename File Selection");
            // Init fragment.
            if(fragmentManager.findFragmentByTag(FileSelectionFragment.TAG) == null ) {
                fragmentManager.beginTransaction()
                        .replace(host_fragment.getId(), FileSelectionFragment.newInstance(), FileSelectionFragment.TAG)
                        .commit();
                item.setChecked(true);
            }
        }else if(id == R.id.nav_rename_task){
            toolbar.setTitle("Rename Task");
            // Init fragment.
            if(fragmentManager.findFragmentByTag(RenameTaskFragment.TAG) == null ) {
                fragmentManager.beginTransaction()
                        .replace(host_fragment.getId(), RenameTaskFragment.newInstance(), RenameTaskFragment.TAG)
                        .commit();
                item.setChecked(true);
            }
        }
        else if(id == R.id.nav_preview) {
            toolbar.setTitle("Rename Preview");
            if (fragmentManager.findFragmentByTag(PreviewFragment.TAG) == null) {
                // Init fragment.
                fragmentManager.beginTransaction()
                        .replace(host_fragment.getId(), PreviewFragment.newInstance(), PreviewFragment.TAG)
                        .commit();
                item.setChecked(true);
            }
        }
        else if(id == R.id.nav_history){
            toolbar.setTitle("Rename History");
            if (fragmentManager.findFragmentByTag(HistoryFragment.TAG) == null) {
                // Init fragment.
                fragmentManager.beginTransaction()
                        .replace(host_fragment.getId(), HistoryFragment.newInstance(), HistoryFragment.TAG)
                        .commit();
                item.setChecked(true);
            }
        }

        drawer.closeDrawer(Gravity.LEFT);
        return true;
    }

    @Override
    public void onProceedToRenameTaskFragment() {
        onNavigationItemSelected(navigationView.getMenu().getItem(2));
    }

    @Override
    public void onProceedToPreviewFragment() {
        onNavigationItemSelected(navigationView.getMenu().getItem(3));
    }

    @Override
    public void onFinishRenameTask() {
        mViewModel_FileSelectList.resetAllVariables();
        mViewModel_FileConfirmList.resetAllVariables();
        mViewModel_TaskList.resetAllVariables();

        mViewModel_PreviewList.savedPreviewToHistory();

        onNavigationItemSelected(navigationView.getMenu().getItem(1));
    }

    @Override
    public void backToTaskCriteria() {
        onNavigationItemSelected(navigationView.getMenu().getItem(2));
    }
}
